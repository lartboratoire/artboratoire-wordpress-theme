<div class="social-media-icons social-media-icons-sharing">

  <a href="https://www.facebook.com/sharer.php?u=<?php echo get_permalink() ?>&t=<?php echo get_the_title() ?>"
     rel="noreferrer" target="_blank" title="Partager l'article sur Facebook">
    <img src="<?php bloginfo('stylesheet_directory'); ?>/ressources/facebook.svg" alt="Partage Facebook">
  </a>

  <a href="https://twitter.com/intent/tweet?text=<?php echo urlencode(get_the_title()) ?>&url=<?php echo urlencode(get_permalink()) ?>&via=artboratoire"
     rel="noreferrer" target="_blank" title="Partager l'article sur Twitter">
    <img src="<?php bloginfo('stylesheet_directory'); ?>/ressources/twitter.svg" alt="Partage Twitter">
  </a>

  <a href="https://pinterest.com/pin/create/link/?url=<?php echo urlencode(get_permalink()) ?>&description=<?php echo urlencode(get_the_title()) ?>+via+l'artboratoire&media=<?php echo the_post_thumbnail_url() ?>"
     rel="noreferrer" target="_blank" title="Partager l'article sur Pinterest">
    <img src="<?php bloginfo('stylesheet_directory'); ?>/ressources/pinterest.svg" alt="Partage via Pinterest">
  </a>

</div>
