<article class="content-wrapper">
  <div class="content">
    <?php
    // getting only one post threw the loop
    if(have_posts()) : ?><?php while(have_posts()) : the_post();
      $post_id = get_the_ID(); ?>

      <header class="content-header">
        <?php // 58rem = max-width of the content :)
        the_post_thumbnail( 'medium_large', ['sizes' => '(max-width: 800px) 70vw, 58rem'] );?>
        <h1 class="header-title"><?php the_title(); ?></h1>
        <p class="header-author">Par <span class="header-author-link"><?php the_author_posts_link(); ?></span></p>
        <?php display_categories_and_tags($post_id) ?>
        <hr>
      </header>

      <?php the_content(); ?>

      <?php if(is_single()): ?>
        <hr>
        <div class="content-cta">
          <button id="share-cta"
                  class="content-cta-button"
                  aria-haspopup="true"
                  aria-expanded="false">Partager l'article</button>
          <p class="btn content-cta-button content-cta-button-right"><a class="hide-link" href="#comments">Accès direct aux commentaires</a></p>
          <?php include('php/templates/cards/popups/popup-sharing.php') ?>
        </div>
      <?php endif; ?>

    <?php endwhile; else: ?>
      <h2 class="card-title">Rah, mince !</h2>
      <p class="card-text">Aucun article n'a été trouvé. <a href="<?php bloginfo('url'); ?>">Retour à l'accueil</a>.</p>
    <?php endif; ?>
  </div>
</article>


<?php
if(is_single()):
  include(get_query_template( 'single-linkedposts' ));

  comments_template();
endif;
?>
