<section id="comments" class="content-wrapper">
	<div class="content">
		<h2 class="comments-title">Commentaires</h2>
		<p class="comments-cta">Vous avez sûrement un avis à partager !</p>
		<p class="comments-instructions">Les champs marqués par un astérisque (*) sont obligatoires.</p>

		<?php
		// a reset of the query is needed for comments to be correctly displayed
		wp_reset_query();

		if ('comments.php' == basename($_SERVER['SCRIPT_FILENAME'])) die ('Do not download this ressource directly; thank you.');

		// if there's a password
		if (!empty($post->post_password)) {
			// and it doesn't match the cookie
			if ($_COOKIE['wp-postpass_' . COOKIEHASH] != $post->post_password) { ?>
				<p><?php _e('Entrer le mot de passe pour voir les commentaires.'); ?></p>
				<?php
				return;
			}
		}

		// if the comments are open (you never know, bud)
		if ('open' == $post->comment_status) : ?>
		  <form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="comment-form">
				<?php
				// checking for connected user

		    if ( isset($user_ID) ) : ?>
		      <p id="connectedas">(Connecté en tant que : <a class="hide-link" href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>.)</p>
					<p class="btn"><a class="hide-link" href="<?php echo get_option('siteurl'); ?>/wp-login.php?action=logout" title="se déconnecter">Se déconnecter</a></p>
		    <?php else : ?>
					<div class="comments-fields">
						<label for="comment-author">Nom / pseudonyme *</label>
			      <input type="text" name="author" id="comment-author" placeholder="Personne"/>
					</div>
					<div class="comments-fields">
		      	<label for="comment-email">Adresse mail (ne sera pas publiée) *</label>
		      	<input type="email" name="email" id="comment-email" placeholder="mail@example.com" />
					</div>
					<div class="comments-fields">
						<label for="comment-url">Site web</label>
		      	<input type="text" name="url" id="comment-url" placeholder="https://example.com/" />
					</div>
		    <?php endif; ?>

				<div class="comments-fields comments-text">
			  	<label for="comment-content">Commentaire inspiré *</label>
		    	<textarea name="comment" id="comment-content" placeholder="Blablabla..."></textarea>
				</div>

		    <input type="hidden" name="comment_post_ID" value="<?php echo $id; ?>" />
				<?php wp_nonce_field( 'comment_nonce' ) ?>

				<?php //do_action('comment_form', $post->ID); ?>

				<input name="submit" type="submit" id="comment-form-submit" tabindex="5" value="Envoyer" />
		  </form>

			<hr>

			<?php
			$post_id = $post->ID;
			// getting the first level comments only
			$args = array(
				'parent'  => '0',
				'number' => 20,
				'order' => 'ASC',
				'post_id' => $post_id,
				'status' => 'approve',
				'include_unapproved' => array(is_user_logged_in() ? get_current_user_id() : wp_get_unapproved_comment_author_email()),
			);

			$comments = get_comments( $args );

			if ($comments) :
				$num_comments = get_comments_number(); ?>
				<div class="comments-content">
					<?php if ( $num_comments == 0 ):
						$comments_title = __('Soyez la.e premier.ère à commenter !');
					elseif ( $num_comments > 1 ):
						$comments_title = __('Lire les commentaires ('. $num_comments .')');
					else:
						$comments_title = __('Lire le commentaire');
					endif;

					echo '<p class="comments-read">'. $comments_title .'</p>';
					?>

			    <div class="comments-content-list">
			      <?php
						foreach($comments as $comment):?>
							<div class="main-comment">
								<?php
								// displaying the first level comments
								include('php/templates/comment.php'); ?>
								<div class="sub-comments">
									<?php
									// getting the sub comments for the current comment
									$args['parent'] = $comment->comment_ID;

									$sub_comments = get_comments( $args );
									foreach($sub_comments as $comment):
										include('php/templates/comment.php');
									endforeach;?>
								</div>
							</div>
						<?php
						endforeach; ?>
					</div>
				</div>
			<?php
			else:?>
					<p>Soyez la.e premier.ère à commenter !</p>
			<?php
			endif; //if($comments)
		endif; // if('open' == $post->comment_status) ?>
	</div>
</section>
