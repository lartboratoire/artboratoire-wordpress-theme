<?php
// getting the taxonomy's name for ACF
$term = get_queried_object();

// checking if the current archive is in the "Aléartoire" category
// all the posts are in it by default
$aleartoire = in_category('aleartoire') && !is_search();

// getting the page number
$page_num = (get_query_var('paged')) ? get_query_var('paged') : 1;
$post_num_low = ($page_num-1) * 10;
$post_num_high = ($page_num-1) * 10 + 10;

// adding a zero in front for a better balance in graphism :)
if($page_num < 10) {
  $page_num = '0' . $page_num;
}
?>

<header class="full-header">
  <?php
  include('php/templates/page-numbers.php');

  $img_id = get_field('taxonomy_img_id', $term);

  if($img_id):
   // getting the image's ID to retrieve its srcset
   $img_src = wp_get_attachment_image_url($img_id);
   // set the srcset with various image sizes
   $img_srcset = wp_get_attachment_image_srcset($img_id);
   // getting the image's maximum width
   $max_width = wp_get_attachment_image_src($img_id, "full")[1] . "px";
   ?>
      <div class="full-header-card card"
           style="max-width: <?php echo $max_width ?>" >
         <img class="wp-post-image"
              src="<?php echo($img_src) ?>"
              srcset="<?php echo($img_srcset) ?>"
              sizes="100vw"
              alt="Avatar de <?php the_author(); ?>" />
  <?php
  else:
    // if there's no image somehow ?>
    <div class="full-header-card card">
  <?php
  endif;

    // getting the revelant title for every occasion :)
    if(is_search()):
      $title = 'Résultat(s) pour : '. get_search_query();
    else:
      $title = single_cat_title('', false);
    endif; ?>

    <h1 class="full-header-title"><?php echo $title ?></h1>

    <?php
    // displays the category description with <p> tags
    echo category_description();

    if ($aleartoire === True):
      include(get_query_template( 'aleartoire-header' ));
    endif;

    $credits = get_field('taxonomy_img_credits', $term);
    if($credits):?>
      <p class="full-header-credits">Crédits : <span class="full-header-credits-link"><?php echo $credits ?></span></p>
    <?php
    endif;?>
  </div>
</header>
