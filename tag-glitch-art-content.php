<?php
// getting the taxonomy's name for ACF
$term = get_queried_object();

// creating several lists with only one loop:
// - the base table of contents
// - its duplicated version (necessary for CSS purposes)
// - the used sources/artists
if( have_rows('glitch_content_blocks', $term) ):
  $toc_list = '<ul>';
  $duplicated_toc_list = '<ul class="duplicated" aria-hidden="true">';
  $sources_list = '<ul>';
  $duplicated_sources_list = '<ul id="duplicated-sources" class="duplicated" aria-hidden="true">';

  while ( have_rows('glitch_content_blocks', $term) ) : the_row();
    $toc_item = '<li><a href="#'. get_sub_field('glitch_title_anchor') .'">'. get_sub_field('glitch_title') .'</a></li>';
    $toc_list .= $toc_item;
    $duplicated_toc_list .= $toc_item;

    $source_item = '<li><a href="'. get_sub_field('glitch_source_url') .'" target="_blank" rel="nofollow">'. get_sub_field('glitch_source_text') .'</a></li>';
    $sources_list .= $source_item;
    $duplicated_sources_list .= $source_item;
  endwhile;

  $toc_list .= '</ul>';
  $duplicated_toc_list .= '</ul>';
  $sources_list .= '</ul>';
  $duplicated_sources_list .= '</ul>';
endif;
?>

<div id="glitch">
  <header class="full-header">
    <a href="#protocol"><h1 class="glitch enter-txt" data-text="glitch://art">glitch://<span id="a">art</span></h1></a>
    <a class="scroll-to-content" href="#protocol" data-text="↓">↓</a>
  </header>

  <section id="protocol" class="glitch-text">
    <h2 id="request">GET art <span class="glitch" data-text="GLITCH">GLITCH</a> 1/1</h2>
    <h3 id="answer">GLITCH 1/1 200 OK</h3>
    <?php
      echo $toc_list;
      echo $duplicated_toc_list;
    ?>
  </section>

  <?php if( have_rows('glitch_content_blocks', $term) ):
    // second loop, necessary if we want to avoid weird-ass string manipulations
    // note: the ID added to the section is later used in CSS to correctly
    // display the fragments around the main image :)
    while ( have_rows('glitch_content_blocks', $term) ) : the_row(); ?>
      <section class="glitch-content" id="<?php the_sub_field('glitch_title_anchor') ?>">
          <div class="wrapper">
            <a href="#">
              <h2><?php the_sub_field('glitch_title') ?></h2>
              <?php if(get_sub_field('glitch_main_image_id')):
                 display_base_image(get_sub_field('glitch_main_image_id'));
                endif;
              ?>

              <?php if( have_rows('fragments') ):
                // gotta loop on those groups
                while ( have_rows('fragments') ) : the_row(); ?>
              <div aria-hidden="true" class="fragment fragment-1">
                <?php if(get_sub_field('glitch_fragment_id_1')):
                   display_base_image(get_sub_field('glitch_fragment_id_1'));
                 endif; ?>
               </div>
               <div aria-hidden="true" class="fragment fragment-2">
                 <?php
                  if(get_sub_field('glitch_fragment_id_2')):
                     display_base_image(get_sub_field('glitch_fragment_id_2'));
                  endif;
                ?>
              </div>
              <?php
                endwhile;
              endif;
              ?>
            </a>

          </div>

          <?php if( have_rows('glitch_links') ): ?>
            <ul>
            <?php while ( have_rows('glitch_links') ) : the_row(); ?>
              <li><a href="<?php the_sub_field('glitch_link_url'); ?>"><?php the_sub_field('glitch_link_text'); ?></a></li>
            <?php endwhile; ?>
            </ul>
          <?php endif; ?>
      </section>
    <?php
    endwhile;
   endif; ?>

   <section id="sources" class="glitch-text">
     <h2>Glitch artistes cités</h2>
     <?php
       echo $sources_list;
       echo $duplicated_sources_list;
     ?>
   </section>

   <p id="quote" lang="en">I would rather be corrupted than repeating the sublime.</p>
</div>
