<noscript>
  <?php
  /*Hidding the hardcoded padding on lazy loaded images, showing the letters
  when JS is disabled */ ?>
  <style>.lazy{padding:0!important;}.lazy-iframe{display:none!important;}.letter{opacity: 1!important;}.tr-hide-wrapper{display:none!important;}
  </style>
</noscript>

<link
  rel="preload"
  as="font"
  type="font/woff2" crossorigin
  href="<?php bloginfo('template_directory'); ?>/assets/styles/fonts/montserrat/montserrat-bold.woff2">

<link
  rel="preload"
  as="font"
  type="font/woff2" crossorigin
  href="<?php bloginfo('template_directory'); ?>/assets/styles/fonts/opensans/opensans-regular.woff2">

<?php
  $css_assets = get_css_assets();
  $js_assets = get_js_assets();
  echo $css_assets[0];
  echo $js_assets[0];
?>

<script>
  <?php
    echo $css_assets[1];
    echo $js_assets[1];
  ?>
  var _paq = window._paq || [];_paq.push(["disableCookies"]);_paq.push(['setDocumentTitle', document.title]);_paq.push(['trackPageView']);_paq.push(['enableLinkTracking']);
  let dnt = (typeof navigator.doNotTrack!=='undefined')?navigator.doNotTrack:(typeof window.doNotTrack!=='undefined')?window.doNotTrack:(typeof navigator.msDoNotTrack!=='undefined')?navigator.msDoNotTrack:null;
  if (1!== parseInt(dnt) && 'yes'!=dnt) {var u="https://lartboratoire.fr/matomo/";_paq.push(['setTrackerUrl', u+'matomo.php']);_paq.push(['setSiteId', '1']);var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'js/index.php'; s.parentNode.insertBefore(g,s);}
</script>
