<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      lang="fr"
			dir="ltr"
			class="smooth-scroll">
<head>
	<!-- Hello there ; this code is open source, take a look here: https://gitlab.com/lartboratoire/artboratoire-wordpress-theme -->

	<title>
		<?php
    if(is_404()):
      _e('404 - Art non trouvé');
    elseif(is_front_page()):
      _e("L'artboratoire - découvrir l'art, le design et la culture autrement");
		else:
      wp_title();
    endif; ?>
	</title>

	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" >
	<link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" >
	<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" >

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" >

	<?php wp_head(); ?>
  <link rel="icon" type="image/png" href="<?php bloginfo('template_directory'); ?>/assets/icons/favicons/favicon-16x16.png" sizes="16x16">
  <link rel="icon" type="image/png" href="<?php bloginfo('template_directory'); ?>/assets/icons/favicons/favicon-32x32.png" sizes="32x32">
  <link rel="apple-touch-icon" href="<?php bloginfo('template_directory'); ?>/assets/icons/favicons/favicon-57x57.png">
	<?php include(get_query_template( 'critical-path' )) ?>

</head>

<body>
		<a class="a11y-hide" href="#top">Accéder directement au contenu.</a>

    <div id="tr-main" aria-hidden="true"></div>

    <noscript>
			<div id="noscript" class="card">
				<h2>L'art a parfois besoin d'un peu d'aide pour être affiché...</h2>
				<p>Ce site fonctionne un peu mieux avec le JavaScript activé, malgré tous mes efforts pour le rendre accessible au maximum. Il ne contient aucun tracker externe, ni de publicité, promis. Si vous le souhaitez, vous pouvez le vérifier par vous-même en consultant son <a href="https://gitlab.com/lartboratoire/artboratoire-wordpress-theme" target="_blank">repo public</a>.
					Vous pouvez aussi visiter <a href="https://lartboratoire.fr/politique-confidentialite/">cette page</a> pour davantage d'informations sur la politique de confidentialité mise en place.</p>
			</div>
		</noscript>

		<?php include(get_query_template( 'menu' )) ?>

		<main data-barba="wrapper">

		 <?php
			$namespace = 'default';

      if(is_front_page()) {
				$namespace = 'front-page';
			} else if(is_single() && !in_category('aleartoire')) {
        // higher priority on single (needs custom scripts)
        // by default, also use the singular namespace's resources
				$namespace = 'single';
      } else if(is_archive() || is_tag() || is_search() || in_category("aleartoire")) {
        $namespace = 'archive';
			} else if(is_singular()) {
        // for other singular-type stuff
        $namespace = 'singular';
      }  else if(is_404()) {
        $namespace = '404';
      }
    ?>

		 <div id="top"
          tabindex="-1"
		      data-barba="container"
		      data-barba-namespace="<?php echo $namespace ?>">
