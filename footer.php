    </div> <!-- barba-container -->
  </main> <!-- barba-wrapper -->


  <footer>
    <div class="footer-social">
      <p class="footer-social-text">Art et artistes sont aussi à découvrir sur les réseaux sociaux</p>
      <div class="footer-social-wrapper">
        <a class="footer-social-icon" href="https://twitter.com/artboratoire" target="_blank">
          <?php include('assets/icons/social/twitter.svg') ?>
        </a>
        <a class="footer-social-icon" href="https://www.facebook.com/artboratoire/" target="_blank">
          <?php include('assets/icons/social/facebook.svg') ?>
        </a>
        <a class="footer-social-icon" href="https://www.pinterest.fr/lartboratoire/" target="_blank">
          <?php include('assets/icons/social/pinterest.svg') ?>
        </a>
      </div>
    </div>

    <div class="footer-informations">
      <div class="footer-informations-legal">
        <p>
          Contact :<br/>
          <span class="email">contact[arobase]lartboratoire[point]fr</span>
        </p>
        <p>
    			ISSN : 2492 - 3095<br/>
    			Copyright &#169; <?php print(date('Y')); ?> <?php bloginfo('name'); ?><br/>
          <a href="https://lartboratoire.fr/mentions-legales/">Mentions légales</a> et <a href="https://lartboratoire.fr/politique-confidentialite/">politique de confidentialité</a>.
    		</p>
      </div>
      <div class="footer-informations-top">
        <a id="back-to-top" class="force-link" href="#top">
        <?php include('assets/icons/arrow-up.svg') ?>
        Haut de page</a>
      </div>
    </div>
  </footer>

</body>
</html>
