<?php
get_header();

include(get_query_template( 'archive-header' ));
include(get_query_template( 'archive-content' ));

get_footer();
?>
