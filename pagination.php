<div class="pagination">
  <?php
  $pagination_classes = 'btn pagination-link';

  $previous_str = 'PAGE PRÉCÉDENTE';
  $next_str = 'PAGE SUIVANTE';

  if(get_previous_posts_link()): ?>
    <p class="<?php echo $pagination_classes ?> pagination-link-previous">
      <a class="hide-link" href="<?php echo get_previous_posts_page_link() ?>">
        <?php include('assets/icons/arrow-up.svg') ?>
        <span class="pagination-link-text"><?php echo $previous_str ?></span>
      </a>
    </p>
  <?php
  else: ?>
    <p class="btn-inactive <?php echo $pagination_classes ?> pagination-link-previous pagination-link-nopost">
      <?php include('assets/icons/arrow-up.svg') ?>
      <span class="pagination-link-text"><?php echo $previous_str ?></span>
    </p>
  <?php
  endif;

  if(get_next_posts_link()): ?>
    <p class="<?php echo $pagination_classes ?> pagination-link-next">
      <a class="hide-link" href="<?php echo get_next_posts_page_link() ?>">
        <span class="pagination-link-text"><?php echo $next_str ?></span>
        <?php include('assets/icons/arrow-up.svg') ?>
      </a>
    </p>
  <?php
  else: ?>
    <p class="btn-inactive <?php echo $pagination_classes ?> pagination-link-next pagination-link-nopost">
      <span class="pagination-link-text"><?php echo $next_str ?></span>
      <?php include('assets/icons/arrow-up.svg') ?>
    </p>
  <?php
  endif; ?>
</div>
