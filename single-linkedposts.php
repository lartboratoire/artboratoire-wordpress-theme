<section class="grid-wrapper linked-posts">
  <h2 class="linked-posts-title">Retrouvez votre chemin</h2>
  <?php display_breadcrumbs(); ?>
  <h2 class="linked-posts-title">Ou continuez l'aventure</h2>
  <div class="grid">
    <?php
    $post_id = get_the_ID();

    $linked_query = new stdClass();
    $post_count = 0;
    // if the user has linked some posts
    if( have_rows('single_linked_posts') ):
      // loop through the rows of data
      while ( have_rows('single_linked_posts') ) : the_row();
        // push the linked post's ID
        $linked_posts_ids[] = get_sub_field('linked_post');
        $post_count++;
      endwhile;

      $args = array(
        'post__in' => $linked_posts_ids,
        'orderby' => 'post__in',
        // needed, else only one fucking post gets retrived omg
        'posts_per_page' => $post_count,
      );
      $linked_query = new WP_Query( $args );
    endif;

    // adding the current post's ID the to-be-excluded articles
    $linked_posts_ids[] = $post_id;

    // if the user has not left some (or all) fields empty
    $nb_posts_to_add = 6 - $post_count;
    // query with the missing number of posts, including the ones already chosen + current post
    $args = array(
      'orderby' => 'rand',
      'posts_per_page' => $nb_posts_to_add,
      'post__not_in' => $linked_posts_ids,
      'post_status' => 'publish',
      'category_name' => 'articles',
      'category__not_in' => array(get_category_id('Devblog'), get_category_id('Translated')) ,
    );

    $padding_query = new WP_Query( $args );

    if(property_exists($linked_query, 'posts')):
      // merging the two queries if some posts have been selected

      //create new empty query and populate it with the other two
      $query = new WP_Query();
      $query->posts = array_merge( $linked_query->posts, $padding_query->posts );

      //populate post_count count for the loop to work correctly
      $query->post_count = $linked_query->post_count + $padding_query->post_count;
    else:
      // if no post has been linked, return the whole padding query instead
      $query = $padding_query;
    endif;

    // now that the big query is ready, getting the g r i d :)
    include(get_query_template( 'grid' )); ?>
  </div>
</section>
