<?php
// getting the current post's categories
$categories = get_the_category();
$cat_name_list = [];
$cat_link_list = [];

foreach ($categories as $cat):
  $cat_name_list[] = $cat->name;
  $cat_link_list[] = '<a class="hide-link" href="'. get_category_link($cat->term_id) .'">'. $cat->name .'</a>';
endforeach;

$cat_query = implode("+", $cat_name_list);
$cat_links = implode(", ", $cat_link_list);

// Thumbnail S T U F F
$img_id = get_post_thumbnail_id();
// getting the image's ID to retrieve its srcset
$img_src = wp_get_attachment_image_url($img_id);
// set the srcset with various image sizes
$img_srcset = wp_get_attachment_image_srcset($img_id);
// getting the image's maximum width
$max_width = wp_get_attachment_image_src($img_id, "full")[1] . "px";
?>

<article class="full-header">
  <div class="full-header-card full-header-card-top card"
       style="max-width: <?php echo $max_width ?>" >
    <img class="wp-post-image"
         src="<?php echo($img_src) ?>"
         srcset="<?php echo($img_srcset) ?>"
         sizes="100vw"
         alt="Avatar de <?php the_author(); ?>" />
    <h1 class="full-header-title full-header-single-title"><?php the_title(); ?></h1>
    <p><?php echo $cat_links ?></p>
  </div>
</article>

<section class="grid-wrapper">
  <div class="grid">

    <?php
    // getting posts in the relevant categories
    $args = array(
      'posts_per_page' => 10,
      // getting posts that have exactly the same categories as the current post
      'category_name' => $cat_query,
      // excluding the current's post from the query (array needed as param)
      'post__not_in' => array(get_the_ID()),
      // queried posts must be older than the current post
      'date_query' => array(
        // getting the current post's time and converting it to an usable format
    		'before'  => date("Y-m-d H:i:s", get_post_time())
    	)
    );

    $query = new WP_Query($args);

    // now that the big query is ready, getting the g r i d :)
    include(get_query_template('grid')); ?>
  </div>
</section>

<div class="pagination">
  <?php
  $pagination_classes = 'btn pagination-link';
  ?>
  <p class="<?php echo $pagination_classes ?> pagination-link-previous">
    <a class="hide-link" href="<?php echo get_category_link($categories[0]->term_id); ?>">
      <?php include('assets/icons/arrow-up.svg') ?>
      <span class="pagination-link-text"/>Vers <?php echo $categories[0]->name; ?></span>
    </a>
  </p>
  <?php
  // it's possible the post only belongs to the Aleartoire category
  // so we need to check if another category has indeed been used
  if(!empty($categories[1])): ?>
    <p class="<?php echo $pagination_classes ?> pagination-link-next">
      <a class="hide-link" href="<?php echo get_category_link($categories[1]->term_id); ?>">
        <span class="pagination-link-text">Vers <?php echo $categories[1]->name; ?></span>
        <?php include('assets/icons/arrow-up.svg') ?>
      </a>
    </p>
  <?php
  else: ?>
    <p class="<?php echo $pagination_classes ?> pagination-link-next">
      <a class="hide-link" href="<?php bloginfo('url'); ?>">
        <span class="pagination-link-text">Retour à l'accueil</span>
        <?php include('assets/icons/arrow-up.svg') ?>
      </a>
    </p>
  <?php
  endif; ?>
</div>
