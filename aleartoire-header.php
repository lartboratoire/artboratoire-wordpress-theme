<h2 class="full-header-subtitle">Sous-catégories</h2>
<ul class="full-header-list">
  <?php
  // saving the current category to do some logic checks later on
  $current_term = get_queried_object();
  $current_term_slug = $current_term->slug;

  // getting the child categories of 'Aleartoire'
  $aleartoire_id = get_category_id("Aleartoire");
  $categories = get_categories(array(
    'child_of' => $aleartoire_id,
    'orderby' => 'name'
  ));

  foreach($categories as $category):
    $category_link = get_category_link($category);
    $category_slug = $category->slug;
    $category_name = $category->name;

    /* cases:
     * - archive:
     * --- current term's slug == aleartoire
     * --- current term's slug == category's slug
    */
    if($current_term_slug == $category_slug):
      $current_class = "highlight";
    else:
      $current_class = "";
    endif;

    echo '
    <li class="full-header-listitem '. $current_class .'">
      <a class="hide-link" href="'. $category_link .'">'. $category_name .'</a>
    </li>';
  endforeach;

  // adding a go back to main category link if inside a subcategory
  if($current_term->term_id !== $aleartoire_id):
    echo '
    <li class="full-header-listitem">
      <a class="hide-link" href="'. get_category_link($aleartoire_id) .'">Retour vers la catégorie générale</a>
    </li>';
  endif;
  ?>
</ul>
