<div class="social-media-icons">
  <a rel="noreferrer" href="https://www.facebook.com/artboratoire/" target="_blank"><img src="<?php bloginfo('stylesheet_directory'); ?>/ressources/facebook.svg" alt="Logo Facebook" /></a>
  <a rel="noreferrer" href="https://twitter.com/artboratoire" target="_blank"><img src="<?php bloginfo('stylesheet_directory'); ?>/ressources/twitter.svg" alt="Logo Twitter" /></a>
  <a rel="noreferrer" href="https://www.pinterest.fr/lartboratoire/" target="_blank"><img src="<?php bloginfo('stylesheet_directory'); ?>/ressources/pinterest.svg" alt="Logo Pinterest" /></a>
</div>
