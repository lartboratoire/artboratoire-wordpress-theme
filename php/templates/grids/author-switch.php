<?php
$template_dir = get_template_directory();

switch($i) {
  case 0:
    include($template_dir . '/php/templates/cards/author.php');
    break;
  case 2:
    include($template_dir . '/php/templates/cards/newsletter.php');
    break;
  case 4:
    include($template_dir . '/php/templates/cards/participate.php');
    break;
  default:
    break;
}
?>
