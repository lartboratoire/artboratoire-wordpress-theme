<?php
$template_dir = get_template_directory();

switch($i) {
  case 0:
    include($template_dir . '/php/templates/cards/art-artists.php');
    break;
  case 2:
    include($template_dir . '/php/templates/cards/themes-techniques.php');
    break;
  case 3:
    include($template_dir . '/php/templates/cards/newsletter.php');
    break;
  case 4:
    include($template_dir . '/php/templates/cards/participate.php');
    break;
  case 5:
    include($template_dir . '/php/templates/cards/aleartoire.php');
    break;
  case 9:
    include($template_dir . '/php/templates/cards/random.php');
    break;
  default:
    break;
}
?>
