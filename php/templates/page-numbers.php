<?php
// getting the page number
$page_num = (get_query_var('paged')) ? get_query_var('paged') : 1;
$post_num_low = ($page_num-1) * 10;
$post_num_high = ($page_num-1) * 10 + 10;

// adding a zero in front for a better balance in graphism :)
if($page_num < 10) {
  $page_num = '0' . $page_num;
}
?>
<p class="full-header-num">Page <?php echo $page_num ?></p>
<p class="full-header-count"><?php echo $post_num_low . ' - ' . $post_num_high ?></p>
