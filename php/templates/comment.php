<article id="comment-<?php comment_ID() ?>" class="comment">
  <h2 class="comment-author"><span class="comment-author-link"><?php comment_author_link() ?></span>, <?php _e('le '); ?><?php comment_date('j F Y') ?> <?php _e('&agrave;');?> <?php comment_time('H:i') ?> : <a class="comment-link hide-link" href="#comment-<?php comment_ID() ?>">#</a></h2>
  <?php if ($comment->comment_approved == '0') : ?>
    <p class="highlight">Nous vérifions le contenu de votre commentaire ; il sera bientôt publié.</p>
  <?php endif; ?>
  <div class="comment-content">
    <?php comment_text() ?>
  </div>
</article>
