<?php
$args = array(
  'orderby' => 'rand',
  'posts_per_page' => '1',
  'category__not_in' => array(get_category_id('Aléartoire')),
);
$sub_query = new WP_Query( $args );
if ( $sub_query->have_posts() ) : while ( $sub_query->have_posts() ) : $sub_query->the_post();
?>
    <article class="card card-with-icon">
      <?php include(get_stylesheet_directory() . '/assets/icons/shuffle.svg'); ?>
      <h2 class="card-title">De l'art au hasard</h2>
      <p class="card-text">Laissez-vous porter par les aléas pour découvrir un article !</p>
      <p class="btn card-cta"><a class="hide-link" href="<?php the_permalink() ?>">C'est parti !</a></p>
    </article>
  <?php
  endwhile;
endif;
?>
