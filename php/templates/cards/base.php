<article class="card">
  <figure>
    <a href="<?php the_permalink(); ?>">
      <?php display_thumbnail_image(get_the_ID(), '(max-width: 800px) 70vw,(max-width: 1200px) 49vw, 33vw'); ?>
    </a>
    <figcaption>
      <h2 class="card-title">
        <a class="hide-link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
      </h2>
      <p class="card-text">
        <?php
        // if the post is in the Aleartoire category, displaying its categories
        // else, displaying its author
        if (in_category('aleartoire')):
          // getting the current post's categories
          $categories = get_the_category();
          $cat_name_list = [];
          $cat_link_list = [];

          foreach ($categories as $cat):
            $cat_name_list[] = $cat->name;
            $cat_link_list[] = '<a class="hide-link" href="'. get_category_link($cat->term_id) .'">'. $cat->name .'</a>';
          endforeach;

          $cat_query = implode("+", $cat_name_list);
          $cat_links = implode(", ", $cat_link_list);

          echo $cat_links;
        else:?>
          <a class="hide-link" href="<?php echo get_author_posts_url(get_the_author_meta( 'ID' )) ?>"><?php echo get_the_author() ?></a>
        <?php endif;?>

      </p>
    </figcaption>
  </figure>
</article>
