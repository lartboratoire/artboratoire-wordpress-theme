<article class="card card-with-icon" id="card-newsletter">
  <?php include(get_stylesheet_directory() . '/assets/icons/letter.svg'); ?>
  <h2 class="card-title">Newsletter artistique</h2>
  <form method="post" action="<?php echo site_url() ?>/?na=ajaxsub" id="newsletter-form" >
    <label for="newsletter-email" class="a11y-hide">Adresse mail pour s'incrire à la newsletter</label>
    <input type="email"
           name="ne"
           id="newsletter-email"
           placeholder="votre adresse mail..."
           required />
    <input type="submit" id="newsletter-submit" value="S'inscrire"/>
  </form>
  <p class="card-text">Art, inspiration, créativité, curiosité</p>
  <p class="card-text">4 jolis mots ; 1 mail par mois maximum</p>
  <p class="card-text">
    <a class="hide-link"
       id="newsletter-more"
       href="#"
       aria-haspopup="true"
       aria-expanded="false">Plus d'informations</a>
  </p>

  <?php include('popups/popup-newsletter.php'); ?>
</article>
