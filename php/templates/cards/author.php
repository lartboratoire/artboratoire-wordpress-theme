<?php
  $user_id = get_the_author_meta('ID');
  $acf_id = 'user_'.$user_id;

  $user_data = get_userdata( $user_id );
  $registered = dateToFrench($user_data->user_registered, "F Y");
?>

<article class="card">
  <figure>
    <?php

    if(get_field('user_avatar_id', $acf_id)):
       // getting the image's ID to retrieve its srcset
       $avatar_id = get_field('user_avatar_id', $acf_id);
       $avatar_src = wp_get_attachment_image_url($avatar_id);
       // set the srcset with various image sizes
       $avatar_srcset = wp_get_attachment_image_srcset($avatar_id);?>
       <img
            class="card-img"
            src="<?php echo($avatar_src) ?>"
            srcset="<?php echo($avatar_srcset) ?>"
            sizes="(min-width: 800px) 500px, 100vw"
            alt="Avatar de <?php the_author(); ?>" />
    <?php
    else: ?>
      <img src="https://lartboratoire.fr/wp-content/themes/artboratoire_v4/ressources/lartboratoire_header_xsmall.jpg"
           alt="Avatar par défaut" />
    <?php
    endif;?>
    <figcaption>
      <h1 class="card-big-title"><?php the_author(); ?></h1>
      <p class="card-text"><?php echo count_user_posts($user_id); ?> articles, depuis <?php echo $registered ?></p>
    </figcaption>
  </figure>

    <?php
    $desc = get_the_author_meta('description');
    if($desc != ""): ?>
      <h2 class="card-title">Présentation</h2>
      <p class="card-text"><?php echo $desc ?></p>
    <?php
    endif; ?>
    <?php
    if( have_rows('user_social_networks', $acf_id) ): ?>
      <h2 class="card-title">Réseaux sociaux</h2>
      <ul class="card-list">
        <?php
        while ( have_rows('user_social_networks', $acf_id) ) : the_row();
          $social_name = get_sub_field('user_social_network_name'); ?>
          <li class="card-list-item">
            <a class="hide-link"
               href="<?php the_sub_field('user_social_network_url'); ?>"
               rel="noreferrer" target="_blank" title="Accéder à : <?php echo $social_name; ?>">
               <?php echo $social_name ?>
            </a>
          </li>
        <?php
        endwhile; ?>
      </ul>
    <?php
    endif; ?>

</article>
