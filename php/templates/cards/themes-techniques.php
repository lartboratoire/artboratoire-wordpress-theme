<article class="card card-with-icon">
  <?php include(get_stylesheet_directory() . '/assets/icons/folder.svg'); ?>
  <h2 class="card-title">L'art par thèmes et techniques</h2>
  <p class="card-text">La diversité artistique est infinie, mais nous vous proposons de découvrir les créateurs par thèmes de prédilection :
    <a class="force-link" href="https://lartboratoire.fr/tag/digital-painting/">digital painting</a>,
    <a class="force-link" href="https://lartboratoire.fr/tag/street-art/">street art</a>,
    <a class="force-link" href="https://lartboratoire.fr/tag/photographie/">photographie</a>,
    <a class="force-link" href="https://lartboratoire.fr/categorie/p-art-ition/">musique</a>,
    <a class="force-link" href="https://lartboratoire.fr/tag/surrealisme/">art surréaliste</a>,
    <a class="force-link" href="https://lartboratoire.fr/tag/aquarelle/">aquarelle</a>,
    <a class="force-link" href="https://lartboratoire.fr/tag/art-abstrait/">art abstrait</a> ou encore
    <a class="force-link" href="https://lartboratoire.fr/tag/sculpture/">sculpture</a> !</p>
</article>
