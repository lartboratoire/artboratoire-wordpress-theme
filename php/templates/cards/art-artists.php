<article class="card card-with-icon">
  <?php include(get_stylesheet_directory() . '/assets/icons/sun.svg'); ?>
  <h2 class="card-title">Art et artistes en folie</h2>
  <p class="card-text">Découvrir des artistes ça vous tente ? L'artboratoire vous offre de quoi faire briller vos mirettes !</p>
  <p class="card-text">Recherche d'inspiration ou besoin de votre dose de créativité quotidienne ? Nous avons sûrement des œuvres et illustrations qui vous plairont.</p>
</article>
