<div class="card-popup-wrapper">
  <div class="card card-popup card-sharing">
    <?php include(get_stylesheet_directory() . '/assets/icons/cross.svg') ?>
    <ul class="card-popup-content card-list">
      <li class="card-list-item">
        <?php include(get_stylesheet_directory() . '/assets/icons/social/facebook.svg') ?>
        <a href="https://www.facebook.com/sharer.php?u=<?php echo urlencode(get_permalink()) ?>&t=<?php echo urlencode(get_the_title()) ?>"
           rel="noreferrer" target="_blank" title="Partager l'article sur Facebook"
           class="card-list-item-content hide-link">Facebook</a>
      </li>
      <li class="card-list-item">
        <?php include(get_stylesheet_directory() . '/assets/icons/social/twitter.svg') ?>
        <a href="https://twitter.com/intent/tweet?text=<?php echo urlencode(get_the_title()) ?>&url=<?php echo urlencode(get_permalink()) ?>&via=artboratoire"
           rel="noreferrer" target="_blank" title="Partager l'article sur Twitter"
           class="card-list-item-content hide-link">Twitter</a>
      </li>
      <li class="card-list-item">
        <?php include(get_stylesheet_directory() . '/assets/icons/social/pinterest.svg') ?>
        <a href="https://pinterest.com/pin/create/link/?url=<?php echo urlencode(get_permalink()) ?>&description=<?php echo urlencode(get_the_title()) ?>+via+l'artboratoire&media=<?php echo the_post_thumbnail_url() ?>"
           rel="noreferrer" target="_blank" title="Partager l'article sur Pinterest"
           class="card-list-item-content hide-link">Pinterest</a>
      </li>
      <li class="card-list-item">
        <?php include(get_stylesheet_directory() . '/assets/icons/copy.svg') ?>
        <input id="sharing-copy-link" class="card-list-item-content card-list-item-input" type="text" value="<?php echo get_permalink() ?>">
      </li>
    </ul>
  </div>
</div>
