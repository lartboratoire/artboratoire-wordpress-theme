<div class="card-popup-wrapper"
     id="newsletter-info"
     aria-labelledby="newsletter-more">
  <div class="card card-popup card-sharing">
    <?php include(get_stylesheet_directory() . '/assets/icons/cross.svg') ?>
    <div class="card-popup-content">
      <p class="card-text">Cette newsletter délivre des informations relatives au contenu de l'artboratoire. Un seul mail au maximum sera diffusé chaque mois, proposant un résumé des dernières publications et, accessoirement, des nouvelles de la vie du site.</p>
      <p class="card-text">Votre adresse mail ne sera pas divulguée à un quelconque opérateur tier ni utilisée à des fins commerciales.</p>
      <p class="card-text"><a class="force-link" href="https://lartboratoire.fr/mentions-legales/">Mentions légales</a> et <a class="force-link" href="https://lartboratoire.fr/politique-confidentialite/#newsletter">politique de gestion des données</a>.</p>
    </div>
  </div>
</div>

<div class="card-popup-wrapper" id="newsletter-success">
  <div class="card card-popup">
    <?php include(get_stylesheet_directory() . '/assets/icons/cross.svg') ?>
    <div class="card-popup-content">
      <?php include(get_stylesheet_directory() . '/assets/icons/smile.svg') ?>
      <h2 class="card-title">Merci pour votre abonnement !</H2>
      <p class="card-text" id="newsletter-success-text"></p>
    </div>
  </div>
</div>

<div class="card-popup-wrapper" id="newsletter-failure">
  <div class="card card-popup">
    <?php include(get_stylesheet_directory() . '/assets/icons/cross.svg') ?>
    <div class="card-popup-content">
      <?php include(get_stylesheet_directory() . '/assets/icons/frown.svg') ?>
      <H2 class="card-title">Une erreur est survenue</h2>
      <p class="card-text" id="newsletter-failure-text"></p>
    </div>
  </div>
</div>
