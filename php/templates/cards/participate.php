<article class="card card-with-icon">
  <?php include(get_stylesheet_directory() . '/assets/icons/pen.svg') ?>
  <h2 class="card-title">Participer</h2>
  <p class="card-text">Ce blog artistique est communautaire : si l'envie vous en dit, vous pouvez <a class="force-link" href="https://lartboratoire.fr/participer/">participer</a> ! Alors n'hésitez pas et à bientôt !</p>
  <p class="btn card-cta"><a class="hide-link" href="https://lartboratoire.fr/participer/">En savoir plus</a></p>
</article>
