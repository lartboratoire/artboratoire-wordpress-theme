<article class="card card-with-icon">
  <?php include(get_stylesheet_directory() . '/assets/icons/painting.svg'); ?>
  <h2 class="card-title">Des milliers d'images</h2>
  <p class="card-text">Aujourd'hui archivée, la bibliothèque d'images de l'artboratoire compte plus de 4500 créations. </p>
  <p class="card-text">Elles n'attendent que vous pour servir d'inspiration ou de source d'émerveillement ! Voici votre petite gorgée d'art à déguster qu'importe le moment de la journée !</p>
  <p class="btn card-cta"><a class="hide-link" href="https://lartboratoire.fr/categorie/aleartoire/">Découvrir la gallerie</a></p>
</article>
