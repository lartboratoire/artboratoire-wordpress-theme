<?php
/* --- NEWSLETTER --- */

/* custom API endpoint adding a new subscriber to Newsletter plugin
Initial source: https://www.thenewsletterplugin.com/forums/topic/ajax-subscription
*/
function newsletter_subscribe() {
	// checking for the nonce
	// it doesn't do much, but it's still a nice thing to have. :)
  check_ajax_referer( 'ajax-nonce', 'nonce', false);

	if(isset($_POST['ne'])) {
		// decoding %## encodings to strings (+ becomes a space)
		$email = urldecode( $_POST['ne'] );

		// just to be sure, you know.
		// we wouldn't want someone to dump the DB somehow
		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			global $wpdb;

			// checking if the email is already in the database
			$exists = $wpdb->get_var(
			 $wpdb->prepare(
				 "SELECT email FROM " . $wpdb->prefix . "newsletter
				 WHERE email = %s",
				 $email
			 	)
		 	);

			if($exists) {
				// if so...
				$output = array(
					'status'    => 'failure',
					'msg'       => 'Vous êtes déjà inscrit à cette newsletter avec cette adresse. Vérifiez votre boîte de messagerie et son dossier "spam".'
				);
			} else {
				NewsletterSubscription::instance()->subscribe();
				$output = array(
					'status'    => 'success',
					'msg'       =>  'Un mail de confirmation va vous être envoyé ; pensez à vérifier votre dossier de spam.'
				 );
			 }
		 	} else {
				$output = array(
					'status'    => 'failure',
					'msg'       => 'Adresse mail invalide. Consultez <a href="https://twitter.com/sila_point" target="_blank">l\'administrateur du site</a> si vous pensez que ce n\'est pas le cas.'
			 	);
		 	}
		} else {
			$output = array(
				'status'    => 'failure',
				'msg'       => 'Une erreur est survenue, champ de mail non renseigné.'
			);
		}
		wp_send_json( $output );
}
// allowing both logged and not logged users to use the endpoint
add_action( 'wp_ajax_nopriv_newsletter_subscribe', 'newsletter_subscribe' );
add_action( 'wp_ajax_newsletter_subscribe', 'newsletter_subscribe' );
