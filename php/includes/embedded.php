<?php
/* Helper functions extracting relevant informations from the WordPress' string
  note: yeah I know oembed_dataparse exists, but I could not get it working
  @param: an HTML string
  @return: the relevant value
*/
function get_embed_width($base_html) {
  preg_match('/width="(.+?)"/', $base_html, $matches);
  return $matches[1];
}

function get_embed_height($base_html) {
  preg_match('/height="(.+?)"/', $base_html, $matches);
  return $matches[1];
}

function get_embed_src($base_html) {
  preg_match('/src="(.+?)"/', $base_html, $matches);
  return $matches[1];
}

function get_video_type($base_html) {
  preg_match('/type="(.+?)"/', $base_html, $matches);
  return $matches[1];
}

/* Overwritting WordPress' [oEmbed] to lazy load the iframes
  @param: the html string provided by WordPress
  @return: a new string with a custom class and a data-src instead of src
*/
function custom_oembed( $base_html ){
  $width = get_embed_width($base_html);
  $height = get_embed_height($base_html);
  $src = get_embed_src($base_html);
  $svg = file_get_contents(get_stylesheet_directory() . '/assets/icons/video.svg');

  /*
    Sadly, we can not use a similar output as the video, because WordPress
    finds it amusing to add some random <p> and <br> tags inbetween the iframe
    :)

    At this point, a random <br> tag still remains (because of the svg icon)
    but it does not really mess with the layout so I'm giving up.

    Side note: <noscript> tags are replaced by <span> tags so the layout is a
    bit weird up w/out JavaScript

    Another note: we're not using some custom padding here to display iframes
    Instead, hardcoding their width/height * 2 seems to get a better display
    Tested with YouTube, Vimeo and soundcloud
  */
  $output = '<div class="lazy lazy-embed lazy-iframe embed-wrapper"
                  >'. $svg .'<iframe class="embed embed-iframe lazy-final" style="max-width: '. $width*2 .'px; height: '. $height*2 .'px" data-src="'. $src .'" allow="fullscreen" allowfullscreen="allowfullscreen"></iframe>'.
            '</div>'
            .'<noscript><iframe class="embed" style="border:0;" src="'. $src .'" allow="fullscreen"></iframe></noscript>';

  return $output;
}
add_filter( 'embed_oembed_html', 'custom_oembed' );

/* Overwritting WordPress' [video] to lazy load the videos
  @param: the html string provided by WordPress
  @return: a new string with a custom class and a data-src instead of src
*/
function custom_video($base_html) {
  $width = get_embed_width($base_html);
  $height = get_embed_height($base_html);
  $src = get_embed_src($base_html);
  $ratio = get_ratio($width, $height);
  $type = get_video_type($base_html);
  $svg = file_get_contents(get_stylesheet_directory() . '/assets/icons/video.svg');

  // Side note: the noscript tag needs to wrap the whole <video>
  $result = '<div class="lazy lazy-embed lazy-video embed-wrapper"
                  style="padding-top: '. $ratio .'%;" >
              '. $svg .'
              <video class="lazy-final embed"
                      preload="metadata"
                      controls="controls"
                      style="max-width: '. $width .'px"
              >
                <source class="video-source"
                        type="'. $type .'"
                        data-src="'. $src .'">
                <a href="'. $src .'">Accéder directement à la vidéo.</a>
              </video>
              <noscript>
                <video class="embed"
                        preload="metadata"
                        controls="controls"
                        style="max-width: '. $width .'px"
                >
                  <source class="video-source"
                          type="'. $type .'"
                          src="'. $src .'">
                  <a href="'. $src .'">Accéder directement à la vidéo.</a>
                </video>
              </noscript>
            </div>';

  return $result;
}
add_filter( 'wp_video_shortcode', 'custom_video');
?>
