<?php
/*
  Everything about the assets loading (scripts and styles).

  *-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*

  Problem: how can we lazy load the relevant styles and scripts for each page
  while keeping a high level of optimisation?
  (ie deferring stuff, cache busting.)

  Answer used here:
  - listing all the assets (either CSS or JS)
  - creating 2 global JS variables (one for CSS assets, the other for JS )
    containing the URLs *and the cache busting parameter associated*
  - adding only the relevant assets based on the context (is the page a single,
    an archive, the front page?)
  - lazy loading the rest on page-change based on the next context, using the
    global CSS/JS variables

  Notes:
  - The context is linked with the "namespace" used by BarbaJS. This one is
    declared inside the `header.php` file.
  - The global JS variables are used in the
    `dev/scripts/components/transitions/logic.js` file.

  How to add a new asset based on a namespace?
  1. Add it the list of assets in load_css_assets() or load_js_assets() below
  2. Add the corresponding namespace (aka same name as in the previous list(s))
     in the header.php logic
  3. Make sure the namespace is correctly triggered when going to that page in
     `dev/scripts/components/transitions/logic.js`.

  *-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*

  Yes, this is some custom enqueing stuff. I know. But at this time, either WP
  does not provide a nice & easy way to do what I'm trying to achieve or I just
  do not know about it.
  If you do, feel free to contact me :)

  *-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*

  the loading madness starts
  right
  here.

  *-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*
*/


/*
  Class used to describe an asset, be it a script or a stylesheet
  Note: maybe using an object's not needed at all. maybe it is. I dunno anymore.

  $name: the asset's name (used to get the relevant file)
  $is_CSS: a boolean
*/
class Asset
{
    private $name;
    private $is_CSS;
    private $asset_file_path;
    private $asset_url;
    private $last_modification;

    public function __construct($name, $is_CSS) {
        $this->name = $name;
        $this->is_CSS = $is_CSS;
        $this->asset_file_path = $this->get_asset_path();

        $this->asset_url = get_bloginfo('template_directory') . $this->asset_file_path;

        $this->last_modification = filemtime(get_stylesheet_directory() . $this->asset_file_path);
    }

    private function get_asset_path() {
      if($this->is_CSS === True) {
        return '/assets/styles/' . $this->name . '.css';
      } else {
        return '/assets/scripts/' . $this->name . '.min.js';
      }
    }

    public function get_complete_url() {
      return $this->asset_url . '?' . $this->last_modification;
    }
}


/*
  *-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*
  CSS assets
  *-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*
*/
function get_css_assets() {
  $css_assets = [
    'core', // default
    '404',
    'archive',
    'front-page',
    'glitch-art',
    'singular',
  ];

  $preload_final = "";
  $js_variable = 'var css_assets = {';

  $contexts = get_css_contexts();

  foreach ($css_assets as $name) {
    $css = new Asset($name, True);

    // only loading the relevant stylesheet, based on the contexts
    if(in_array($name, $contexts)) {
      $base_link  = '<link href="'. $css->get_complete_url() .'" class="css-'. $name .'" ';

      $preload_final .= $base_link . 'as="style" rel="stylesheet">';
    }

    $js_variable .= "'$name': '". $css->get_complete_url() ."',";
  }

  // Do not forget to close the JS var
  $js_variable .= '};';

  return [$preload_final, $js_variable];
}


function get_css_contexts(){
  // core is always included by default
  $contexts = ['core'];

  if(is_front_page()) {
    $contexts[] = "front-page";
  } else if(is_singular() && !in_category('aleartoire')) {
    // for:
    // - page
    // - single
    // - but not aleartoire whatsoever

    // also, the front page does not need CSS rules for the content
    // which is why "else if" is used
    $contexts[] = "singular";
  } else if(is_404()) {
    $contexts[] = "404";
  }

  if(is_archive() || is_tag() || is_search() || in_category("aleartoire")) {
    $contexts[] = "archive";
  }

  return $contexts;
}


/*
  *-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*
  JavaScript assets
  *-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*
*/
function get_js_assets() {
  $js_assets = [
    'core', // default
    'libs', // default
    '404',
    'aleartoire',
    'front-page',
    'glitch-art',
    'single'
  ];

  $script_tags = '';
  $js_variable = 'var js_assets = {';

  $contexts = get_js_contexts();

  foreach ($js_assets as $name) {
    $js = new Asset($name, False);

    if(in_array($name, $contexts)) {
      $script_tags .= '<script src="'. $js->get_complete_url() .'" defer="defer"
                      class="js-'. $name .'"></script>';
    }

    $js_variable .= "'$name': '". $js->get_complete_url() ."',";
  }

  // Do not forget to close the JS var
  $js_variable .= '};';

  return [$script_tags, $js_variable];
}

function get_js_contexts() {
  // core and libs are always included by default
  $contexts = ['core', 'libs'];

  if(is_front_page()) {
    $contexts[] = 'front-page';
  }

  if(is_single() && !in_category('aleartoire')) {
    // the aleartoire singles do not need any extra JS
    $contexts[] = 'single';
  }

  if(is_404()) {
    $contexts[] = '404';
  }

  return $contexts;
}
?>
