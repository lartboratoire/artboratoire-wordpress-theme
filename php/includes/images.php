<?php
/* --- General purpose functions related to images --- */

/* enabling the thumbnails
 * everyone likes thumbnails
 * why wouldn't you add them?
*/
if ( function_exists( 'add_theme_support' ) ) {
  add_theme_support( 'post-thumbnails' );
}

/* Doubling the maximum srcset size for images
 * so full screen image doesn't get blurry
*/
function new_srcset_max($max_width) {
    return 4096;
}
add_filter('max_srcset_image_width', 'new_srcset_max');


/* Disabling multiple sizes for .gif images
 * source: https://wordpress.stackexchange.com/questions/229675/disable-resizing-of-gif-when-uploaded
 * note: this is needed as using srcset for lazy loaded content does not work well with .gif
* e.g: only the main uploaded gif works, its other sizes are static images :/
*/
function disable_upload_sizes( $sizes, $metadata ) {

    // Get filetype data.
    $filetype = wp_check_filetype($metadata['file']);

    // Check if is gif.
    if($filetype['type'] == 'image/gif') {
        // Unset sizes if file is gif.
        $sizes = array();
    }

    // Return sizes you want to create from image (None if image is gif.)
    return $sizes;
}
add_filter('intermediate_image_sizes_advanced', 'disable_upload_sizes', 10, 2);


/* --- GLOBAL VARIABLES --- */
// used to create the base64 placeholders and the canvas element
$GLOBALS['base64Pixels'] = 5;
// enlarging the canvas based on the initial image's width is an easy way
// to blur the placeholder (the image is stretched in javascript with the
// width and height attributes of the canvas element)
$GLOBALS['blurRatio'] = 6;
$GLOBALS['canvasWidth'] = $GLOBALS['base64Pixels'] * $GLOBALS['blurRatio'];

/**
 * Creating a base64 $GLOBALS['base64Pixels'] by $GLOBALS['base64Pixels'] pixels max version of every image (png, jpg,
 * animated gifs...)
 * Works on upload and on update (regenerate thumbnail plugin)
 *
 * @param  array metadatas
 * @param  int attachement's id
 * @return array
 */
function add_base64_media_filter($metadata, $attachment_id) {
  // only generating the base64 image once
  if($metadata['image_meta']['base64'] == "") {
    // get_attached_file() retrieves the path of an attachment
    // realpath() returns canonicalized absolute pathname
    $path = realpath(get_attached_file($attachment_id));
    $size = getimagesize($path);

    // if the file is not an image, its size === 0
    if($size !== false) {
      $image = new Imagick($path);
      $type = pathinfo($path, PATHINFO_EXTENSION);

      // if dealing with a gif, extracting the first image
      // to use it as thumbnail
      if($type == "gif") {
        $image = $image->coalesceImages();

        foreach ($image as $frame) {
          $gif_thumbnail = new Imagick();
          $gif_thumbnail->addImage($frame->getImage());
          $image = $gif_thumbnail;
          break;
        }
      }

      //  If TRUE is given as a third parameter
      // then columns and rows parameters are used as maximums for each side
      $image->thumbnailImage($GLOBALS['base64Pixels'], $GLOBALS['base64Pixels'], true);

      // updating the relevant field with the base64 version of the image
      $base64str = 'data:image/' . $type . ';base64,' . base64_encode($image);
      update_post_meta($attachment_id, 'base64', $base64str);

      // cleaning up the mess before leaving
      $image->clear();
    }
  }

  // do not forget to return this, we're in a filter after all
  return $metadata;
}
add_filter('wp_update_attachment_metadata', 'add_base64_media_filter', 10, 2);

/**
 *   Adding a new, hidden, custom field to the attachments
 *
 * @param  array form fields
 * @param  WPpost the attachment
 * @return array
 */
function my_add_attachment_base64_field( $form_fields, $post ) {
   $field_value = get_post_meta( $post->ID, 'base64', true );
   $form_fields['base64'] = array(
       'value' => $field_value ? $field_value : '',
       'label' => __( 'base64' ),
       'input' => 'hidden',
       'helps' => __( 'Set a base64 for this attachment' )
   );
   return $form_fields;
}
add_filter( 'attachment_fields_to_edit', 'my_add_attachment_base64_field', 10, 2 );


/* --- Helpers used for the lazy loaded content --- */

/* Function retrieving the image's URL and the relevant CSS
 * params: image's ID, a boolean (true = add some padding-top)
 * return: an array of 4 strings, array([0] => 'url', [1] => 'base64', [2] => 'style', [3] => canvasHeight )
           or False if the image does not exist
*/
function get_lazy_content_infos($img_id, $with_padding, $size) {
  // getting the img url and size
  // format: $img_array( [0] => [url] [1] => [width] [2] => [height] [3] => [useless_bool])
  $img_array = wp_get_attachment_image_src($img_id, $size);
  // if the caption is used on a non existing image, abort mission
  if(!$img_array) {
    return False;
  }

  // getting the previously stored base64 version of the image
  $img_base64 = get_post_meta($img_id, 'base64');
  // if no base64 version of the image exists,
  // going for a solo pixel #323232 fallback
  if(empty($img_base64)) {
    $img_base64 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4wEGFDcHUUOL2wAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAAADElEQVQI12MwMjICAAEwAJc5VdcKAAAAAElFTkSuQmCC";
  } else {
    $img_base64 = esc_html($img_base64[0]);
  }

  // not all images need padding for the style
  $padding = '';
	$width = $img_array[2];
	$height = $img_array[1];

  if($with_padding) {
    $padding = 'padding-top: '. get_ratio($width, $height) .'%;';
  }

  // it's important to take the orientation into account
  $canvas_height = 0;
	if($width > $height) {
    // note: the round function is needed as using floating number for
    // height or width attributes on canvas is not W3C compliant
		$canvas_height = round($GLOBALS['canvasWidth'] * $height / $width);
	} else {
		$canvas_height = round($GLOBALS['canvasWidth'] * $width / $height);
	}

  return array($img_array[0], $img_base64, $padding, $canvas_height);
}

/* Custom function used returning the lazy loaded image template
 * params: the post's ID used an HTML id (helps tracking which image to use during
 *         page transitions), the image's ID
 * return: an HTML string
 * used for: aleartoire content post, shortcodes (base/gallery)
*/
function get_lazy_img($post_id, $img_id, $sizes = '') {
  $img_infos = get_lazy_content_infos($img_id, true, 'full');
  if($img_infos === False) {
    return '';
  }

  // getting the alt attribute of the image
  $img_alt = get_post_meta( $img_id, '_wp_attachment_image_alt', true);

  // checking if the image needs to be blurred because of NSFW tag
  // this is used here as it's related to captions AND galleries
  $nsfw_class = "";
  if (get_field( 'nsfw_img', $img_id)) {
    $nsfw_class = ' nsfw';
  }

  $data_sizes = '';
  $default_sizes = '';
  if($sizes) {
    $default_sizes = 'sizes="'. $sizes .'"';
    $data_sizes = 'data-sizes="'. $sizes .'"';
  } else {
    $default_sizes = 'sizes="100vw"';
    $data_sizes = 'data-sizes="100vw"';
  }

  $src_set = wp_get_attachment_image_srcset($img_id);

  /*
  note: the src="data:," is used to remove an empty request on the document
  on the first load (yeah browsers are weird)
  */
  return '<div class="lazy lazy-img '. $nsfw_class .'"
						style="'. $img_infos[2] .'">
						<img
							class="lazy-base64"
							src="' .$img_infos[1]. '"
              alt="Placeholder"
              aria-hidden="true"
						>
            <canvas
							class="lazy-placeholder"
							width="'. $GLOBALS['canvasWidth'] .'"
							height="'. $img_infos[3] .'"
              aria-hidden="true">
						</canvas>
						<img
							class="lazy-final"
              data-id="'. $post_id .'"
              src="data:,"
							data-src="'. $img_infos[0] .'"
							data-srcset="'. $src_set .'"
              '. $data_sizes .'
							alt="'. wptexturize($img_alt) .'"
						>
						<noscript>
							<img
								src="'. $img_infos[0] .'"
                srcset="'. $src_set .'"
								alt="'. wptexturize($img_alt) .'"
                '. $default_sizes .'
							>
						</noscript>
				 </div>';
}

/* Wrapper function used for images for which IDs are already known
 * basically only used in the glitch art tag at this point in time
 * params: the image's ID
 * return: . (echo)
*/
function display_base_image($img_id) {
  echo get_lazy_img($img_id, $img_id, '');
}

/* Wrapper function used to display thumbnails as lazy loaded <img> tags
 * NOTE: this is needed as it's nativaly used in other php files
 * params: the post's ID
 * return: . (echo)
*/
function display_thumbnail_image($post_id, $sizes) {
  echo get_lazy_img($post_id, get_post_thumbnail_id($post_id), $sizes);
}


function get_thumbnail_background($post_id, $wrapper_class, $size) {
  $thumbnail_id = get_post_thumbnail_id($post_id);
  $thumbnail_infos = get_lazy_content_infos($thumbnail_id, false, $size);
  if($thumbnail_infos === False) {
    return '';
  }

  return '
      <div class="lazy lazy-div '. $wrapper_class .'" aria-hidden="true">
        <img
          class="lazy-base64"
          src="' .$thumbnail_infos[1]. '"
          alt="Placeholder"
        >
        <canvas
          class="attachment-post-thumbnail"
          width="'. $GLOBALS['canvasWidth'] .'"
          height="'. $thumbnail_infos[3] .'">
        </canvas>
        <div
          class="attachment-post-thumbnail lazy-final"
          data-src="'. $thumbnail_infos[0] .'"
          data-id="'. $post_id .'"
        >
        </div>
      </div>
    ';

}

/* --- Highjacking WordPress captions (lazy load and some other things) --- */

/* Function creating the HTML string used to display captions (single img + gallery img)
 * params: img's id, img's legend (<figcaption> tag's content)
 * return: a HTML string for the img only
 */
function get_caption_output($img_id, $caption_text_content) {
  $is_full_screen = get_field( 'full_screen_img', $img_id);
  $html = '';

  if($is_full_screen) {
    // closing the content's div and opening it again after the full screen image :)
    $html .= '</div>
            <figure class="content-figure full-screen">'
              . get_lazy_img($img_id, $img_id) . '

              <figcaption class="full-screen-figcaption image-caption">'. wptexturize($caption_text_content) .'</figcaption>
            </figure>
            <div class="content">';
  } else {
    $width = wp_get_attachment_image_src($img_id, 'full')[1];
    $html .= '<figure class="content-figure" style="max-width: '.$width.'px">'.
                get_lazy_img($img_id, $img_id)
                .'<figcaption class="image-caption">'. wptexturize($caption_text_content) .'</figcaption>
              </figure>';
  }

  return $html;
}

/* Overwritting WordPress' [caption] shortcode to remove the hardcoded width attribute
 * Also adding custom classes to images with the "full width" ACF option activated
 * Also adding extra padding to get some sweet lazy loaded images
 * #YOLO
 * doc: https://codex.wordpress.org/Plugin_API/Filter_Reference/img_caption_shortcode
*/
function custom_img_caption_shortcode( $empty, $attr, $content ){
  // getting the caption shortcode's attributes
  $attr = shortcode_atts( array(
		'id'      => '',
		'align'   => 'alignnone',
		'width'   => '',
		'caption' => ''
	), $attr );

	if ( 1 > (int) $attr['width'] || empty( $attr['caption'] ) ) {
		return '';
	}

  // init various values used in the final ouput
  $caption_container_classes = 'wp-img-container';
  $caption_text_classes = 'wp-img-text';
  $img_ratio = 0;
  $img_array = [''];

	if ( $attr['id'] ) {
    // do not forget to escape HTML chars
    $text_attachment_id = esc_attr($attr['id']);

    // getting the img's ID, stored in the first cell of $id_matches
    // format: "attachment_XXXXXX"
    preg_match('/[0-9]+$/',$text_attachment_id, $id_matches);
    $img_id = $id_matches[0];

    return get_caption_output($img_id, $attr['caption']);
	} else{
    return '';
  }
}
add_filter( 'img_caption_shortcode', 'custom_img_caption_shortcode', 10, 3 );

/* Overwritting WordPress' [gallery] shortcode to change the layout
 * --- deleting DL / DT, adding some divs around
 * --- no more float and hardcoded clear:both on full width
 * --- no randomly added CSS
 * --- which means, every type of [gallery] will now be the same
 * --- more french fries for the people
 * --- Also, lazy load.
*/
function custom_post_gallery_shortcode( $output, $attr) {
    global $post, $wp_locale;

    static $instance = 0;
    $instance++;

    // We're trusting author input, so let's at least make sure it looks like a valid orderby statement
    if ( isset( $attr['orderby'] ) ) {
        $attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
        if ( !$attr['orderby'] )
            unset( $attr['orderby'] );
    }

    extract(shortcode_atts(array(
        'order'      => 'ASC',
        'orderby'    => 'menu_order ID',
        'id'         => $post->ID,
        'size'       => 'full',
        'include'    => '',
        'exclude'    => ''
    ), $attr));

    $id = intval($id);
    if ( 'RAND' == $order )
        $orderby = 'none';

    if ( !empty($include) ) {
        $include = preg_replace( '/[^0-9,]+/', '', $include );
        $_attachments = get_posts( array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );

        $attachments = array();
        foreach ( $_attachments as $key => $val ) {
            $attachments[$val->ID] = $_attachments[$key];
        }
    } elseif ( !empty($exclude) ) {
        $exclude = preg_replace( '/[^0-9,]+/', '', $exclude );
        $attachments = get_children( array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
    } else {
        $attachments = get_children( array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
    }

    if ( empty($attachments) )
        return '';

    if ( is_feed() ) {
        $output = "\n";
        foreach ( $attachments as $att_id => $attachment )
            $output .= wp_get_attachment_link($att_id, $size, true) . "\n";
        return $output;
    }

    foreach ( $attachments as $id => $attachment ) {
      if (trim($attachment->post_excerpt)) {
        $output .= get_caption_output($id, $attachment->post_excerpt);
      }
    } // foreach

    return $output;
}
add_filter( 'post_gallery', 'custom_post_gallery_shortcode', 10, 2 );
