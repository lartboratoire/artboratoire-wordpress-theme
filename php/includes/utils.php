<?php
/* Function used to get the exact padding needed for lazy loaded content
 * params: img's width and height
 * return: ratio (integer value)
*/
function get_ratio($width, $height) {
  if($height == 0) {
    return 0;
  }
  return intval($width) / intval($height) * 100;
}

?>
