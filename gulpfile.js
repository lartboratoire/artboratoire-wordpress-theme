/* -----------------------------------------------------------------------------
                          The npm n i g h t m a r e
  ------------------------------------------------------------------------------*/
const gulp         = require('gulp');

const merge        = require('merge-stream');

const sass         = require('gulp-dart-sass');

const cleanCSS     = require('gulp-clean-css');
const header       = require('gulp-header');

const babel        = require('gulp-babel');
const browserify   = require('browserify');

const uglifyes     = require('uglify-es');
const composer     = require('gulp-uglify/composer');
const uglify       = composer(uglifyes, console);

const source       = require('vinyl-source-stream');
const buffer       = require('vinyl-buffer');

const concat       = require('gulp-concat');
const rename       = require('gulp-rename');


/* -----------------------------------------------------------------------------
                                CSS stuff
------------------------------------------------------------------------------*/
/*
  --- Core ---
*/
// const cssCorePath      = 'dev/style/core.scss';
// const cssCoreHeader = "\
// /*\n\
// Theme Name: L'artboratoire\n\
// Theme URI: https://lartboratoire.fr\n\
// Author: sila.\n\
// Author URI: https://twitter.com/sila_point\n\
// Description: This was supposed to be a cool theme but something went terribly wrong. :/\n\
// Version: 5.0\n\
// License: GNU General Public License v2 or later\n\
// License URI: https://www.gnu.org/licenses/gpl-2.0.html\n\
// Text Domain: artboratoire\n\
// */\n\
// "
//
// /* Function building the CSS for the style.css file (mandatory for WordPress) */
// function cssCore() {
//   return gulp.src(cssCorePath)
//     .pipe(sass({outputStyle: 'compressed'}))
//     .pipe(header(cssCoreHeader))
//     .pipe(rename('style.css'))
//     .pipe(gulp.dest('.'));
// }


/*
  --- Assets ---
*/
const cssInputPath = 'dev/style/routes/';
const cssAssetsWatchPath  = cssInputPath + "*.scss";
const cssOutputPath = 'assets/styles/';

const cssAssetsPaths = [
  {
    src: 'dev/style/core.scss',
    dest: cssOutputPath
  },
  {
    src: cssInputPath + 'front-page/front-page.scss',
    dest: cssOutputPath
  },
  {
    src: cssInputPath + 'singular/singular.scss',
    dest: cssOutputPath
  },
  {
    src: cssInputPath + 'archive/archive.scss',
    dest: cssOutputPath
  },
  {
    src: cssInputPath + 'glitch-art/glitch-art.scss',
    dest: cssOutputPath
  },
  {
    src: cssInputPath + '404/404.scss',
    dest: cssOutputPath
  },
];

/* Function building the CSS from the various SCSS assets */
function cssAssets() {
  let tasks = cssAssetsPaths.map(function(asset){
    return gulp.src(asset.src)
      .pipe(sass({outputStyle: 'compressed'}))
      .pipe(gulp.dest(asset.dest));
  });

  return merge(tasks);
}


/*
  --- Watch ---
*/
const cssWatchPath = 'dev/style/**/**/*.scss';

function cssWatch() {
  gulp.watch(cssWatchPath, cssAssets);
}


/* -----------------------------------------------------------------------------
                                  JS stuff
------------------------------------------------------------------------------*/
/*
  --- Core ---
*/
const jsCoreConfig = {
  src: 'dev/scripts/core.js',
  dest: 'assets/scripts/',
  name: 'core.min.js',
};

function jsCore() {
  const b = browserify({
    entries: jsCoreConfig.src,
    debug: true
  })
  .transform('babelify', { presets: ["@babel/preset-env"] });

  return b.bundle()
    .pipe(source(jsCoreConfig.name))
    .pipe(buffer())
    .pipe(uglify())
    .pipe(gulp.dest(jsCoreConfig.dest));
}


/*
  --- Assets---
*/
const jsInputPath = 'dev/scripts/routes/';
const jsOutputPath = 'assets/scripts/';

const jsAssetsPaths = [
    {
      src: jsInputPath + 'front-page.js',
      dest: jsOutputPath,
      name: 'front-page.min.js'
    },
    {
      src: jsInputPath + 'single.js',
      dest: jsOutputPath,
      name: 'single.min.js'
    },
    {
      src: jsInputPath + 'glitch-art.js',
      dest: jsOutputPath,
      name: 'glitch-art.min.js'
    },
    {
      src: jsInputPath + '404.js',
      dest: jsOutputPath,
      name: '404.min.js'
    },
];

function jsAssets() {
  let tasks = jsAssetsPaths.map(function(element){
    let b = browserify({
      entries: element.src,
      debug: true
    })
    .transform('babelify', { presets: ["@babel/preset-env"] });

    return b.bundle()
      .pipe(source(element.name))
      .pipe(buffer())
      .pipe(uglify())
      .pipe(gulp.dest(element.dest));
  });

  return merge(tasks);
}


/*
  --- Libraries ---
*/
const jsLibConfig = {
  src: 'dev/scripts/libs/*.js',
  dest: 'assets/scripts/',
  name: 'libs.min.js'
};

/* Function used to concat all the libs code into one minified file */
function jsLib() {
  return gulp.src(jsLibConfig.src)
    .pipe(uglify())
    .pipe(concat(jsLibConfig.name))
    .pipe(gulp.dest(jsLibConfig.dest));
}


/*
  --- Watch ---
*/
const jsWatchPath = 'dev/scripts/**/*.js'

function jsWatch() {
  gulp.watch(jsWatchPath, gulp.parallel(jsCore, jsAssets));
}


/* -----------------------------------------------------------------------------
                                f i n a l l y
------------------------------------------------------------------------------*/
// starting by building everything (including JS libraries)
// then watching css and js
const start = gulp.series(
                gulp.parallel(cssAssets, jsCore, jsAssets, jsLib),
                gulp.parallel(cssWatch, jsWatch)
              );

/*
* Export a default task
*/
exports.default = start;
