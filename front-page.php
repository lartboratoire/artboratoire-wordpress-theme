<?php get_header(); ?>

<header class="front-header">
  <h1 class="front-title" aria-label="l'artboratoire">
    <span class="letter" aria-hidden="true">l</span><span class="letter" aria-hidden="true">'</span><span class="highlight front-title-art" aria-hidden="true"><span class="letter">a</span><span class="letter">r</span><span class="letter">t</span></span><span class="letter" aria-hidden="true">b</span><span class="letter" aria-hidden="true">o</span><span class="letter" aria-hidden="true">r</span><span class="letter" aria-hidden="true">a</span><span class="letter" aria-hidden="true">t</span><span class="letter" aria-hidden="true">o</span><span class="letter" aria-hidden="true">ı</span><span class="letter" aria-hidden="true">r</span><span class="letter" aria-hidden="true">e</span><span class="highlight letter" aria-hidden="true">.</span>
  </h1>
  <h2 class="front-subtitle">
    <a class="hide-link" href="#content"><span>Découvrir</span> <span class="front-subtitle-change">l'art</span> <span>autrement</span></a>
  </h2>

  <div class="front-header-wrapper">
    <?php include(get_query_template( 'front-header-img' )); ?>
  </div>
</header>


<div class="grid-wrapper" id="content" tabindex="-1">
  <div class="grid">
    <?php
    $args = array(
      'posts_per_page' => 10,
      'category_name' => 'articles'
    );

    $query = new WP_Query( $args );
    include(get_query_template( 'grid' )); ?>
  </div>
</div>
<?php get_footer(); ?>
