<?php
  //  setting some base values used later on
  $user_id = get_the_author_meta('ID');
  $acf_id = 'user_'.$user_id;

  $user_data = get_userdata( $user_id );
  $registered = dateToFrench($user_data->user_registered, "M Y");

?>

<header class="full-header">
  <p class="full-header-num">Page <?php echo $page_num ?></p>
  <p class="full-header-count"><?php echo $post_num_low . ' - ' . $post_num_high ?></p>

  <?php
  if(get_field('user_avatar_id', $acf_id)):
     // getting the image's ID to retrieve its srcset
     $avatar_id = get_field('user_avatar_id', $acf_id);
     $avatar_src = wp_get_attachment_image_url($avatar_id);
     // set the srcset with various image sizes
     $avatar_srcset = wp_get_attachment_image_srcset($avatar_id);?>
  <div class="author-header-content">
      <h1><?php the_author(); ?></h1>
      <p><?php echo count_user_posts($user_id); ?> articles, depuis <?php echo $registered ?></p>

    <?php
    $desc = get_the_author_meta('description');
    if($desc != ""): ?>
      <h2>Présentation</h2>
      <p><?php echo $desc ?></p>
    <?php
    endif; ?>

    <?php
    if( have_rows('user_social_networks', $acf_id) ): ?>
      <h2>Réseaux sociaux</h2>

      <?php // loop through the rows of data
      while ( have_rows('user_social_networks', $acf_id) ) : the_row();
        $social_name = get_sub_field('user_social_network_name');
        ?>
        <a href="<?php the_sub_field('user_social_network_url'); ?>"
           rel="noreferrer" target="_blank" title="Accéder à : <?php echo $social_name; ?>">
        <?php if($social_name != 'Site personnel'): ?>
          <img src="<?php bloginfo('stylesheet_directory'); ?>/ressources/<?php echo strtolower($social_name); ?>.svg" alt="Compte <?php echo $social_name; ?>">
        <?php else: ?>
          <img src="<?php bloginfo('stylesheet_directory'); ?>/ressources/user.svg" alt="Site personnel">
        <?php endif; ?>
        </a>
      <?php endwhile; ?>

      </div>
    <?php endif; ?>

    <p class="author-cta"><a class="scroll-to-content" href="#content">Articles récents</span> / <a href="https://lartboratoire.fr/participer/">Participer</a></p>
  </div>


      <img class="enter-img wp-post-image"
           src="<?php echo($avatar_src) ?>"
           srcset="<?php echo($avatar_srcset) ?>"
           sizes="(min-width: 800px) 500px, 100vw"
           alt="Avatar de <?php the_author(); ?>" />
    <?php else: ?>
      <img class="enter-img wp-post-image"
           src="https://lartboratoire.fr/wp-content/themes/artboratoire_v4/ressources/lartboratoire_header_xsmall.jpg"
           alt="Avatar par défaut" />
    <?php endif; ?>
  </div>

</header>
