<?php
// General grid template

$i = 0;

if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();

  if(is_front_page()) {
    include('php/templates/grids/front-page-switch.php');
  } else if(is_author()) {
    include('php/templates/grids/author-switch.php');
  } else {
    include('php/templates/grids/default-switch.php');
  }

  include('php/templates/cards/base.php');

  $i++;

  endwhile;
else : ?>
  <div class="card">
    <h2 class="card-title">Rah, mince !</h2>
    <p class="card-text">Ce que vous cherchez ne se trouve pas ici, navré. <a class="force-link" href="<?php bloginfo('url'); ?>">Retour à l'accueil</a>.</p>
  </div>
<?php endif; ?>
