<?php
/* This template is used for the main archive content, displaying posts from the defaults categories.
 * /!\ It also handles search results and tags (cf search.php and tag.php for their respective inclusions).
 * (For the aleartoire category, please refer to the archive-aleartoire-content.php file.)
*/
?>

<section class="grid-wrapper">
  <div class="grid">
    <?php
    // setting up the query args with pagination/number of posts to query
    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

    $args = array(
      'posts_per_page' => 10,
      'paged' => $paged
    );

    // getting the current category ID, tag ID or search term
    $category = get_query_var( 'cat' );
    $tag  = get_query_var('tag');
    $search = get_query_var('s');
    $author = get_query_var('author');

    // based on the results of get_query_var
    // adding either the category, the tag or the search term to the query args
    if($category) {
      $args['cat'] =  $category;
    } else if($tag) {
      $args['tag'] =  $tag;
    } else if($search){
      $args['s'] = $search;
    } else {
      $args['author'] = $author;
    }

    $query = new WP_Query( $args );
    include(get_query_template( 'grid' )); ?>
  </div>

  <?php include(get_query_template( 'pagination' )); ?>
</section>
