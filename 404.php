<?php get_header(); ?>


<?php
  $page_id = get_page_by_title('Custom 404');
  $img_id = get_field('404_image_id', $page_id);

  $img_src = wp_get_attachment_image_url($img_id);
  // set the srcset with various image sizes
  $img_srcset = wp_get_attachment_image_srcset($img_id);
?>

<article class="error-content-wrapper">
  <div class="error-content">
    <h1 class="error-title"><span class="error-title-line highlight"><span class="letter">4</span><span class="letter">0</span><span class="letter">4</span></span> <span class="error-title-line"><span class="letter">a</span><span class="letter">r</span><span class="letter">t</span></span> <span class="error-title-line"><span class="letter">i</span><span class="letter">n</span><span class="letter">c</span><span class="letter">o</span><span class="letter">n</span><span class="letter">n</span><span class="letter">u</span></span></h1>

    <div class="tr-hide-wrapper" aria-hidden="true">
      <div class="tr-hide"></div>
    </div>

    <img class="error-img"
         src="<?php echo($img_src) ?>"
         srcset="<?php echo($img_srcset) ?>"
         sizes="100vw"
         alt="Le Penseur de Rodin réfléchissant sur sa condition" />
  </div>
  <p class="error-subtitle"><?php the_field('404_text', $page_id); ?></p>
  <p class="error-return"><a class="force-link" href="<?php bloginfo('url'); ?>">Retour à l'accueil</a></p>
</article>
<?php get_footer(); ?>
