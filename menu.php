<nav id="nav">
  <div class="menu card-container">
    <a id="logo" class="hide-link" href="<?php bloginfo('url'); ?>">
      <p>
        l'<span class="highlight">art</span>boratoire<span class="highlight">.</span>
      </p>
    </a>
    <button id="menu-button" aria-haspopup="true" aria-expanded="false">menu</button>
  </div>
  <div id="main-menu" class="menu-card">
    <ul>
        <li class="item"
            id="categories"
            aria-haspopup="true"
            aria-expanded="false"
            >
          Catégories
        </li>
        <li class="item"
            id="themes"
            aria-haspopup="true"
            aria-expanded="false">
          Thèmes
        </li>
        <li class="item"
            id="informations"
            aria-haspopup="true"
            aria-expanded="false">
          Informations
        </li>
        <li class="item"
            id="search"
            aria-haspopup="true"
            aria-expanded="false">
          Rechercher
        </li>
        <li class="item ignore-click">
          <a id="item-get-involved"
             class="force-link ignore-click"
             href="https://lartboratoire.fr/participer/">
            Participer
          </a>
        </li>
    </ul>
  </div>

  <div class="menu-card" id="categories-card">
    <div class="menu-card-description">
      <?php include('assets/icons/cross.svg') ?>
      <p class="menu-card-description-text">Catégories</p>
    </div>
    <ul aria-labelledby="categories">
      <?php display_menu_terms("cat"); ?>
    </ul>
  </div>

  <div class="menu-card" id="themes-card">
    <div class="menu-card-description">
      <?php include('assets/icons/cross.svg') ?>
      <p class="menu-card-description-text">Thèmes</p>
    </div>
    <ul aria-labelledby="themes">
      <?php display_menu_terms("tag"); ?>
    </ul>
  </div>

  <div class="menu-card" id="informations-card">
    <div class="menu-card-description">
      <?php include('assets/icons/cross.svg') ?>
      <p class="menu-card-description-text">Informations</p>
    </div>
    <ul aria-labelledby="informations">
      <li class="item">
        <a class='hide-link' href='https://lartboratoire.fr/a-propos/'>
          À propos
        </a>
      </li>
      <li class="item">
        <a class='hide-link' href='https://lartboratoire.fr/politique-confidentialite/'>
          Politique de confidentialité
        </a>
      </li>
      <li class="item">
        <a class='hide-link' href='https://lartboratoire.fr/mentions-legales/'>
          Mentions légales
        </a>
      </li>
      <li class="item">
        <a class='hide-link' href='https://lartboratoire.fr/categorie/dev/'>
          Devblog
        </a>
      </li>
    </ul>
  </div>

  <div class="menu-card" id="search-card">
    <div class="menu-card-description">
      <?php include('assets/icons/cross.svg') ?>
      <form method="get"
            action="<?php bloginfo('url'); ?>/"
            role="search"
            aria-labelledby="search"
            id="menu-search-form">
        <label for="search-input" class="a11y-hide">Valeur de la recherche</label>
        <input type="text"
               value="<?php the_search_query(); ?>"
               placeholder="Ex: aenami"
               name="s"
               id="search-input" />
        <input type="submit"
               value="rechercher"
               id="search-submit"/>
      </form>
    </div>
  </div>
</nav>
