<?php
get_header();

// this file is only used as an hook base on the category
// each article in the "aleartoire" cat. also belongs to child categories so we only need to test it on the parent
if ( in_category( 'aleartoire')):
  include(get_query_template( 'single-aleartoire' ));
else:
  include(get_query_template( 'singular-base' ));
endif;

get_footer();
?>
