<?php
/*
While it would be possible to get every variable used there in PHP, this image
is not supposed to change much, so we're only using the automated resize
and optimisation techniques from WordPress

Various notes:
- The svg and foreignObject's widths/heights are based on the selected image.
- The eyes positionning is based on the svg container
- This does not need JavaScript
- This is responsive
- omg how COOL is that?

PS: it seems "sclera" is the name of the eye's white part
*/

// getting the taxonomy's name for ACF
$term = get_queried_object();
$img_id = get_field('front_page_img_id', $term);

if($img_id):
  // getting the image's ID to retrieve its srcset
  $img_src = wp_get_attachment_image_url($img_id);
  // set the srcset with various image sizes
  $img_srcset = wp_get_attachment_image_srcset($img_id); ?>

  <div class="tr-hide-wrapper" aria-hidden="true">
    <div class="tr-hide"></div>
  </div>
  <svg id="front-header-svg"
      class="tr-to-reveal"
      width="1698"
      height="2560"
      viewBox="0 0 1698 2560"
      preserveAspectRatio="xMidYMid slice"
      fill="none"
      xmlns="http://www.w3.org/2000/svg">

      <foreignObject x="0" y="0" width="1698" height="2560" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility">
          <img id="front-header-img"
          src="<?php echo $img_src ?>"
          srcset="<?php echo $img_srcset ?>"
          sizes="(max-width: 800px) 80vw, 45vw"
          alt="<?php echo get_post_meta($img_id , '_wp_attachment_image_alt', true); ?>"
          />
      </foreignObject>

      <ellipse class="sclera" cx="510" cy="1190.5" rx="90" ry="51.5"/>
      <circle class="pupil" id="pupil-left" cx="475.5" cy="1195" r="26.5"/>
      <ellipse class="sclera" cx="981" cy="1120.5" rx="82" ry="66.5"/>
      <circle class="pupil" id="pupil-right" cx="953.5" cy="1135" r="28.5"/>
  </svg>

<?php
endif;?>
