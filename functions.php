<?php

require_once 'php/includes/utils.php';
require_once 'php/includes/images.php';
require_once 'php/includes/embedded.php';
require_once 'php/includes/newsletter.php';
require_once 'php/includes/assets.php';


/* --- REMOVING NATIVE WORDPRESS STUFF --- */
/* Removing rel="EditURL" link */
remove_action ('wp_head', 'rsd_link');
/* Removing rel="wlwmanifest" link */
remove_action( 'wp_head', 'wlwmanifest_link');
/* Removing meta generator leaking the WordPress' version */
remove_action('wp_head', 'wp_generator');
/* Removing the useless dns prefetching (all resources come from the same DN) */
remove_action( 'wp_head', 'wp_resource_hints', 2);

// Completely removing the admin bar
add_filter('show_admin_bar', '__return_false');

/* Removing Gutemberg blocks' css */
function wps_deregister_styles() {
    wp_dequeue_style( 'wp-block-library' );
}

add_action( 'wp_print_styles', 'wps_deregister_styles', 100 );
/* Removing emoji script / css */
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

/* Removing dashicons css */
function wpdocs_dequeue_dashicon() {
    wp_deregister_style('dashicons');
}
add_action( 'wp_enqueue_scripts', 'wpdocs_dequeue_dashicon' );

/* Removing newsletter plugin css */
add_filter('newsletter_enqueue_style', '__return_false');


/* Adding global variables to be used in JavaScript
  this is needed because we can not wp_localize_script() as the app.min.js script is
  lazy loaded (cf critical-path.php)
  source: https://wordpress.stackexchange.com/questions/119573/is-it-possible-to-use-wp-localize-script-to-create-global-js-variables-without-a
*/
function add_js_variables(){
    ?>
    <script>
    var ajaxUrl = <?php echo json_encode( admin_url( "admin-ajax.php" ) ) ?>;
    var ajaxNonce = <?php echo json_encode( wp_create_nonce( "ajax-nonce" ) ) ?>;
    </script>
    <?php
}
add_action ( 'wp_head', 'add_js_variables' );


/* --- COMMENTS --- */

/* Function adding the target blank / rel no referrer to links in comments
*/
function filter_comment_content( $comment_content ){
    return str_replace( "<a ", "<a target='_blank' rel='noreferrer' ", $comment_content );
}
add_filter( "comment_text", "filter_comment_content" );

/* Function removing the IP field of WordPress' comments (yay privacy!)
*/
function remove_commentsip( $comment_author_ip ) {
  return '';
}
add_filter( 'pre_comment_user_ip', 'remove_commentsip' );


function my_verify_comment_nonce() {
    check_admin_referer( 'comment_nonce' );
}
add_action( 'pre_comment_on_post', 'my_verify_comment_nonce' );


/* --- MISC --- */

/* get the category's ID based on his name
 * params: string
 * return: Term Row (object or array) from database
 * false if not found
*/
function get_category_id($cat_name){
	$term = get_term_by('name', $cat_name, 'category');
  if( !is_object($term)) {
    return;
  }
  return $term->term_id;
}

/* Function used to retrieve the current post's categories and tags
   params: the post's ID
   return: . (echo)
*/
function display_categories_and_tags($post_id) {
  $tags = get_the_tags($post_id);
  $categories = get_the_category($post_id);

  if($tags == "") {
    $list = $categories;
  } else {
    $list = array_merge($categories, $tags);
  }

  $html = "<ul class='cat-list'>";
  foreach ($list as $key => $el) {
    $html .= '<li class="cat-items"><a class="hide-link" href="'. get_term_link($el->term_id) .'">' . $el->name . '</a></li>';
  }
  $html .= "</ul>";
  echo $html;
}

/*
  Helper filter to get only the relevant categories (excluding "Articles")
*/
function exc_cat($cats) {
  //not on admin pages
  if(!is_admin()){
    $exc = array('Articles');
    foreach ($cats as $i=>$cat){
      if(in_array($cat->name, $exc)){
        unset($cats[$i]);
      }
    }
  }
  // as we removed one item from the initial array, we need to reorder stuff
  // around before returning so the new array is not messed up
  return array_values($cats);
}
add_filter('get_the_categories', 'exc_cat');

/* function used to display the menu's categories and tags
 * params: (string)
 * returns: .
*/
function display_menu_terms($type) {
  $terms = array();

  // retrieving a list of terms, either categories or tags
  if($type === "cat") {
    $id_articles = get_category_id("Articles");
    $id_devblog = get_category_id("Devblog");
    $id_translated = get_category_id("Translated");

    $to_exclude = array($id_articles, $id_devblog, $id_translated);

    // parent>0: removes sub categories (interesting for Aleartoire)
    $terms = get_categories(array(
      'orderby' => 'name',
      'parent'   => 0,
      'exclude' => $to_exclude
    ));
  } else {
    $terms = get_tags( array(
      'orderby' => 'count',
      'order' => 'DESC',
      'posts_per_page'   => -1,
    ) );
  }

  $html = "";
  foreach( $terms as $term ) {
    $term_link = get_term_link($term->term_id);
    $name = $term->name;
    $slug = $term->slug;

    // getting the last post published for the current term
    if($type === "cat") {
      $post_args = array(
        'posts_per_page' => 1,
        'category_name' => $slug,
        'post_status' => 'publish',
      );
    } else {
      $post_args = array(
        'posts_per_page' => 1,
        'tag' => $slug,
        'post_status' => 'publish',
      );
    }

    $query = new WP_Query( $post_args );
    $posts = $query->posts;
    // note: foreach needed as it's an object
    // also there's only post so it's fine :)
    foreach ($posts as $post) {
      $post_link = get_permalink($post->ID);
      $post_title = $post->post_title;

      $html .= "<li class='item'>
                  <div class='item-wrapper'>
                      <p class='item-title'>
                        <a class='hide-link' href='{$term_link}' title=\"Catégorie : {$name}\">{$name}</a>
                      </p>
                    <p class='item-post-title'>
                      <a class='hide-link' href='{$post_link}'>{$post_title}</a>
                    </p>
                  </div>
                  <a class='hide-link' href='{$post_link}'>
                    ". get_thumbnail_background($post->ID, 'item-post-img', 'medium') ."
                  </a>
                </li>";
    }

    wp_reset_postdata();
  }

  echo $html;
}


/* function used to display the first category of posts in the "aleartoire" category_name
 * params: an array of categories objects retrived from get_the_category() WP function
 * returns: the category's name || "Aléartoire"
*/
function get_aleartoire_category($categories) {
  if(empty($categories[1])):
    $category_name = "Aléartoire";
  else:
    $category_name = $categories[1]->name;
  endif;

  return $category_name;
}


function display_breadcrumbs() {
  echo '<p class="breadcrumbs">';
  echo '<a class="force-link" href="'. home_url() .'" rel="nofollow">Accueil</a>';
  if (is_category() || is_single()) {
    $categories = get_the_category();
    if ( ! empty( $categories ) ) {
      echo " → ";
    	echo '<a class="force-link" href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>';
    }

    if (is_single()) {
      echo " → ";
      the_title();
    }
  } elseif (is_page()) {
    echo " → ";
    echo the_title();
  }

  echo '</p>';
}

// Convert a date or timestamp into French.
function dateToFrench($date, $format) {
    $english_days = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
    $french_days = array('lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche');
    $english_months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
    $french_months = array('janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre');
    return str_replace($english_months, $french_months, str_replace($english_days, $french_days, date($format, strtotime($date) ) ) );
}

/* --- Custom file size upload --- */
@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );
?>
