This repository is archived. Up-to-date theme for lartboratoire.fr is now available on [GitHub](https://github.com/artboratoire/artboratoire.github.io). 

# A random french art blog

This repository contains the code used or the [artboratoire](https://lartboratoire.fr)'s WordPress Theme. This is a documentation for curious people who want to know what's going on behind the scenes.

## Contributing / reporting an issue
This code is for sure NOT perfect. If you run into an horrible bug/security hole, feel free to open an issue or to hit me up on [Twitter](https://twitter.com/sila_point).

I hope everything's OK but if you spot something wrong, contact me. Please. I'll buy you a beer or something.

## Quick install
If you want to use the theme, you need:
- A web server, such as Apache or Nginx
  - PHP 7+ (tested on 7.0, 7.1, 7.2, 7.3, 7.4)
  - Imagemagick for PHP (ex : [php7-imagick](https://wiki.archlinux.org/index.php/PHP#Imagemagick))
- [WordPress](https://wordpress.org/latest.zip) (tested from 4.5 to latest)
  - [Advanced Custom Fields](https://www.advancedcustomfields.com/) (PRO)

## Quick dev
The build process uses [gulp](https://gulpjs.com/) to automate various tasks (compiling, minifying, etc). See `gulpfile.js` for details.

- Install nodeJS/npm. Using [nvm](https://github.com/nvm-sh/nvm) is pretty cool. I am personnaly using the Long Time Support (v14.16).
- Install the arious dependencies found `package.json` with `npm install`.
- Launch gulp from the command line: `gulp` or `./node_modules/.bin/gulp`.

## Architecture and design choices

I am a noob: here's how I roll.

### External libraries/tools

#### JavaScript libraries

- [Barba.js](https://barbajs.org/): a (more or less) light-weight page transition library (cf. `dev/scripts/components/transitions/`)
- [Intersection observer polyfill](https://github.com/w3c/IntersectionObserver/tree/master/polyfill): self exp.
- [Anime.js](https://animejs.com/): a light-weight animation library

#### Other tools

- [Matomo](https://matomo.org/): an opensource, self-hosted, privacy respectful analytics solution (cf `dev/scripts/transitions/main-transitions.js`).
- [Newsletter plugin](https://www.thenewsletterplugin.com/): a self-hosted newsletter solution for WordPress (cf `dev/scripts/components/newsletter/`)

More general consideration about the privacy policy, in french, can be found [here](https://lartboratoire.fr/politique-confidentialite/).


### PHP/templates

- Along with the basic WordPress hierarchy, I've also used some custom templates (found in `php/templates`), mainly to create the grid and its cards.
- The `php/includes` folder is used to organise the code included inside the `functions.php` file. This helps reduce its size and organise better.

### Javascript

Let's assume we are currently in the `dev/scripts` directory, shall we?
- `core.js`: main-but-kept-clean file  
- `components/`: scripts corresponding to actual HTML elements or group of elements
- `toolkit/`: scripts used in various parts of the app, as helpers

For a clearer approach to components, the code has been split in two:
1. `[component]/logic.js`: getting DOM elements, if/else madness and overall logic stuff.
2. `[component]/animations.js`: only for graphical modifications.

### CSS

As SCSS is used during the development phase, variables can be (and have been) used. You can check 'em out at `dev/style/toolkit/_variables.scss`. At this point, no real CSS variables (custom properties such as `var(--main-color)`) are in use. Maybe later, if a secondary theme is provided. For now, using the pre-compiled SCSS is better performance-wise.

Also, most of the files are broken into smaller components, based on media queries. There are included in the parent inside the relevant query:
```
[rest of the default file]

@media screen and (min-width:[breakpoint]px){
  @import 'components/[component]/[component]-[breakpoint]';
}
```
This keeps all the files nice and clean, while going for the mobile-first experience. Yay.

### Lazy loading images, videos and iframes

On upload, a 6 by 6 pixels version of the initial image is created and the base64 encoded translation is stored in WordPress' database (see `php/includes/images.php`, `add_base64_media_filter`). This string is then used in the HTML . On load, the base64 image is added inside a canvas stretching and automatically blurring it. Thanks to an IntersectionObserver, the full resolution image is loaded when the placeholder comes into view (see `dev/scripts/components/lazy.js`).

This system is pretty close to the one used by Medium (check [this article](https://jmperezperez.com/medium-image-progressive-loading-placeholder/) out for some more specific information). That being said, my sweet baby has the merit to save one HTTP request as the base64 version of the image is already inside the HTML. The server only has to deal with the final, full-resolution image request.

Note: while videos and iframes do not get their own blurry background, the lazy loading method is the same.

### Assets loading: the JS/CSS clown fiesta

What do?
- Every asset (be it a JS or CSS file) needs to be deferred for performance purposes, which is not well supported by the default enqueueing system used by WordPress.
- The correct assets also have to be loaded based on the current context/template (e.g.: a different CSS/JS code is needed for the front page or an archive page) so only the minimal code is loaded.
- The cache needs to be correctly handled (busted if needed, based on the asset's last modification).
- This has to work with BarbaJS transitions, which do not reload the `head`.

Basically, I wrote my own way to enqueue scripts. It's probably suboptimal development wise, but it's super sexy and articulates very well with BarbaJS.

The solution to this multifaceted problem is available in the following files:
- `php/includes/assets.php`: generating tags and two global JS variables;
- `critical-path.php` : adding said tags and variable to the `head`;
- `dev/scripts/components/transitions/logic.js`: loading the asset based on the namespace on page transition.

## Final words

Please forgive the often broken english. It's not my birth language, and I ran out of coffee 3 weeks ago. If you have any question about the tools used or this code (it's hard to actually explain the organisation, as I've had my head into it for the last 2/3 months), hit me up!
