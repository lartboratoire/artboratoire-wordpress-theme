import {PopUp} from '../popup/logic'
import {swapValue} from './animations'


export function start() {
  let newsletterCard = document.getElementById('card-newsletter')
  if(newsletterCard) {
    new PopUp(document.getElementById('newsletter-info'),
              document.getElementById('newsletter-more'),
              document.getElementById('newsletter-more'))
  }

  let form = document.getElementById('newsletter-form')
  if(form) {
    form.addEventListener("submit", (e) => {
      e.preventDefault()
      formSubmission(form)
    })
  }
}


/*
  Function doing the sweet AJAX call to admin-ajax.php (action: newsletter_subscribe) in order to add a new subscriber
*/
function formSubmission(form) {
  // disabling the input so the user does not spam the hell out of it
  // UX, kinda.
  let emailInput = document.getElementById('newsletter-email')
  emailInput.disabled = true

  let email = emailInput.value

  let req = new XMLHttpRequest()
  req.open('POST', ajaxUrl, true)
  req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded")
  // Sending the action, the email and the nonce for safety bonus points
  // Note: the "ne" parameter is mandatory for it to work (cf source code: newsletter/subscription/subscription.php)
  // Doc: https://www.thenewsletterplugin.com/documentation/subscription/newsletter-forms/
  req.send("action=newsletter_subscribe&ne="+email+"&nonce="+ ajaxNonce);

  let submitInput = document.getElementById('newsletter-submit')
  let previousValue = submitInput.value

  // feedback on submit
  swapValue(submitInput, 'En cours...')

  req.onreadystatechange = () => {
    if(req.readyState === 4) {
      if(req.status === 200) {
        if(!req.response) {
          displayError('La réponse est vide.')
        } else {
          let res = JSON.parse(req.response)
          displayMsg(res.msg, res.status)
        }
      } else {
        displayError('Oups, voilà qui est fâcheux.')
      }
    }
    
    // do not forget to revert the previous state
    swapValue(submitInput, previousValue)
    emailInput.disabled = false
  } // onreadystatechange
}


function displayMsg(msg, type) {
  let pTag = document.getElementById('newsletter-'+ type +'-text')
  pTag.innerHTML = msg

  let pop = new PopUp(document.getElementById('newsletter-' + type))
  pop.open()
}
