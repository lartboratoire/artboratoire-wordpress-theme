export function swapValue(submitInput, newValue) {
  let duration = 200
  /* First thing: a11y */
  if(window.config.prefersReducedMotion) {
    duration = 0
  }

  let tl = anime.timeline({
    easing: 'easeInOutCirc'
  });

  tl.add({
    targets: submitInput,
    duration: duration,
    opacity: [1, 0],
    complete: () => {
      submitInput.value = newValue
    }
  })

  tl.add({
    targets: submitInput,
    duration: duration,
    opacity: [0, 1],
  })
}
