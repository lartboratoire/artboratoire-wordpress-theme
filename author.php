<?php
// header.php
get_header();?>

<header class="full-header">
  <?php
  include('php/templates/page-numbers.php'); ?>
</header>

<?php
include(get_query_template( 'archive-content' ));

get_footer();
?>
