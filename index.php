<?php get_header(); ?>

<article class="content-wrapper">
  <div class="content">
    <?php
    if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>
      <header class="content-header">
        <h1 class="header-title"><?php the_title(); ?></h1>
        <hr>
      </header>

      <?php the_content(); ?>
      
    <?php endwhile; else: ?>
        <h2>Rah, mince !</h2>
        <p>Le contenu de cette page n'a pas été trouvé. <a href="<?php bloginfo('url'); ?>">Retour à l'accueil</a>.</p>
    <?php endif; ?>
  </div>
</article>

<?php get_footer(); ?>
